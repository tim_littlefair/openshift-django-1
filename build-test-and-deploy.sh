#!/bin/sh

sh ~/bin/oc-login

ENV='LD_LIBRARY_PATH=/opt/rh/rh-python35/root/usr/lib64'

if ~/bin/oc start-build tl --follow
then
    echo Build OK
    if ~/bin/oc deploy tl --follow
    then
        echo Deploy OK
        pod_name=`oc get pods | grep "Running" | awk '/tl/ { print $1 }'`
        ~/bin/oc exec $pod_name -- sh -c "$ENV python3 manage.py collectstatic --noinput"
    else
        echo Deploy failed
    fi
else
    echo Build failed
fi

 
