"""tl_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url  # , include
# from django.contrib import admin
from welcome.views import index as welcome_index
from tl_welcome.views import index as tl_index
from tl_welcome.views import health as tl_health
from tl_trailer.urls import urlpatterns as tl_trailer_urlpatterns
urlpatterns = [
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', tl_index),
    url(r'^health$', tl_health),
    url(r'^welcome$', welcome_index),
    # url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += tl_trailer_urlpatterns
