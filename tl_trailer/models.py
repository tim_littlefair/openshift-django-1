from django.db import models
import random

# We want to assign random numeric ids which are exactly
# the same length without requiring zero padding.
# We do this by selecting a power of 10 N and allowing
# ids in the range 10**(N-1) .. (10**N) -1
# By default we choose N=6 giving us id's in the range 100000 to 999999.
_DEFAULT_ID_DIGITS = 6
_id_digits = _DEFAULT_ID_DIGITS


def _gen_id():
    return random.randrange(10**(_id_digits-1), 10**_id_digits)


# We want id's to be sparsely allocated, so we set a maximum
# capacity per object type equal to the minimum of the id
# range.  The number of possible id values in the id range
# is exactly nine times the minimum value, so id allocation
# density can never rise above one ninth.
def _capacity():
    return 10**(_id_digits-1)


# For test purposes, we want to be able to vary the id length
# to make it easier to write tests which exercise what happens
# when we reach capacity
def set_id_digits_for_test(test_id_digits):
    global _id_digits
    if(test_id_digits):
        _id_digits = test_id_digits
    else:
        # Passing None or 0 can be used to reset the
        # length to the default
        _id_digits = _DEFAULT_ID_DIGITS


class CapacityError(RuntimeError):
    def __init__(self, limit, classname):
        RuntimeError.__init__(
            self,
            "This maximum capacity of %d objects of type %s reached" % (
                limit, classname,
            )
        )


def save_with_unique_id(obj):
    saved_objects_of_same_type = type(obj).objects
    count = saved_objects_of_same_type.count()
    if count >= _capacity():
        raise CapacityError(_capacity(), type(obj))
    while(saved_objects_of_same_type.filter(id=obj.id).exists()):
        obj.id = _gen_id()
    obj.save()


# Add an extra utility function to the Django model class
# before inheriting from it.
def class_name(self):
    return self.__class__.__name__
models.Model.class_name = class_name

#"""
class Owner(models.Model):
    id = models.IntegerField(primary_key=True, default=_gen_id)
    display_name = models.CharField(max_length=1024)

    def __str__(self):
        return self.display_name


class Trail(models.Model):
    id = models.IntegerField(primary_key=True, default=_gen_id)
    owner = models.ForeignKey(Owner, null=True, on_delete=models.SET_NULL)
    expiry_date = models.DateField(null=True)
    locality = models.CharField(max_length=1024)
    description = models.CharField(max_length=1024)

    def __str__(self):
        return self.locality


class Zone(models.Model):
    id = models.IntegerField(primary_key=True, default=_gen_id)
    # Zones are owned by Trails so they do not need
    # their own owner and expiry date fields
    trail = models.ForeignKey(Trail, on_delete=models.CASCADE)
    name = models.CharField(max_length=1024)
    address = models.CharField(max_length=1024)
    url = models.CharField(max_length=1024)
    description = models.CharField(max_length=1024)
    latitude = models.FloatField()
    longitude = models.FloatField()
    radius_metres = models.FloatField()
    southern_latitude = models.FloatField()
    western_longitude = models.FloatField()
    northern_latitude = models.FloatField()
    eastern_longitude = models.FloatField()

    def __str__(self):
        return self.name


class Group(models.Model):
    id = models.IntegerField(primary_key=True, default=_gen_id)
    owner = models.ForeignKey(Owner, null=True, on_delete=models.SET_NULL)
    expiry_date = models.DateField(null=True)
    name = models.CharField(max_length=1024)
    notes = models.CharField(max_length=1024, null=True)
    latitude_min = models.FloatField(default=-90.0)
    longitude_min = models.FloatField(default=-180.0)
    latitude_max = models.FloatField(default=+90.0)
    longitude_max = models.FloatField(default=+180.0)
    subgroups = models.ManyToManyField("self", symmetrical=False)
    trails = models.ManyToManyField(Trail)

    def __str__(self):
        return self.name
# """