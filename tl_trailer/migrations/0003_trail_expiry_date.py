# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tl_trailer', '0002_auto_20170611_0352'),
    ]

    operations = [
        migrations.AddField(
            model_name='trail',
            name='expiry_date',
            field=models.DateField(null=True),
        ),
    ]
