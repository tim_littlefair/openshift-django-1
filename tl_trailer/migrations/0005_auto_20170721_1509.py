# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tl_trailer', '0004_auto_20170706_1525'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subgrouptogroupassociation',
            name='group',
        ),
        migrations.RemoveField(
            model_name='subgrouptogroupassociation',
            name='subgroup',
        ),
        migrations.RemoveField(
            model_name='trailtogroupassociation',
            name='group',
        ),
        migrations.RemoveField(
            model_name='trailtogroupassociation',
            name='trail',
        ),
        migrations.AddField(
            model_name='group',
            name='subgroups',
            field=models.ManyToManyField(to='tl_trailer.Group', related_name='_group_subgroups_+'),
        ),
        migrations.AddField(
            model_name='group',
            name='trails',
            field=models.ManyToManyField(to='tl_trailer.Trail'),
        ),
        migrations.AlterField(
            model_name='group',
            name='latitude_max',
            field=models.FloatField(default=90.0),
        ),
        migrations.AlterField(
            model_name='group',
            name='latitude_min',
            field=models.FloatField(default=-90.0),
        ),
        migrations.AlterField(
            model_name='group',
            name='longitude_max',
            field=models.FloatField(default=180.0),
        ),
        migrations.AlterField(
            model_name='group',
            name='longitude_min',
            field=models.FloatField(default=-180.0),
        ),
        migrations.DeleteModel(
            name='SubgroupToGroupAssociation',
        ),
        migrations.DeleteModel(
            name='TrailToGroupAssociation',
        ),
    ]
