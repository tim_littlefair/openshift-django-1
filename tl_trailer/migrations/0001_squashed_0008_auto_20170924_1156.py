# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    replaces = [('tl_trailer', '0001_initial'), ('tl_trailer', '0002_auto_20170611_0352'), ('tl_trailer', '0003_trail_expiry_date'), ('tl_trailer', '0004_auto_20170706_1525'), ('tl_trailer', '0005_auto_20170721_1509'), ('tl_trailer', '0006_auto_20170722_0430'), ('tl_trailer', '0007_auto_20170723_0635'), ('tl_trailer', '0008_auto_20170924_1156')]

    dependencies = [
    ]

    operations = [
    ]
