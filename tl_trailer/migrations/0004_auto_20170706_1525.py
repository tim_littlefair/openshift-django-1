# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tl_trailer.models


class Migration(migrations.Migration):

    dependencies = [
        ('tl_trailer', '0003_trail_expiry_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='zone',
            name='eastern_longitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='zone',
            name='northern_latitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='zone',
            name='southern_latitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='zone',
            name='western_longitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='group',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False, default=tl_trailer.models._gen_id),
        ),
        migrations.AlterField(
            model_name='owner',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False, default=tl_trailer.models._gen_id),
        ),
        migrations.AlterField(
            model_name='trail',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False, default=tl_trailer.models._gen_id),
        ),
        migrations.AlterField(
            model_name='zone',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False, default=tl_trailer.models._gen_id),
        ),
    ]
