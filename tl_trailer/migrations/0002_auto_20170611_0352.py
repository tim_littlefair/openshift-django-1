# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tl_trailer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=1024)),
                ('latitude_min', models.FloatField()),
                ('longitude_min', models.FloatField()),
                ('latitude_max', models.FloatField()),
                ('longitude_max', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('display_name', models.CharField(max_length=1024)),
            ],
        ),
        migrations.CreateModel(
            name='SubgroupToGroupAssociation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('group', models.ForeignKey(to='tl_trailer.Group')),
                ('subgroup', models.ForeignKey(related_name='subgroup_id', to='tl_trailer.Group')),
            ],
        ),
        migrations.CreateModel(
            name='Trail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('locality', models.CharField(max_length=1024)),
                ('description', models.CharField(max_length=1024)),
                ('owner', models.ForeignKey(to='tl_trailer.Owner', null=True, on_delete=django.db.models.deletion.SET_NULL)),
            ],
        ),
        migrations.CreateModel(
            name='TrailToGroupAssociation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('group', models.ForeignKey(to='tl_trailer.Group')),
                ('trail', models.ForeignKey(to='tl_trailer.Trail')),
            ],
        ),
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=1024)),
                ('address', models.CharField(max_length=1024)),
                ('url', models.CharField(max_length=1024)),
                ('description', models.CharField(max_length=1024)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('radius_metres', models.FloatField()),
                ('trail', models.ForeignKey(to='tl_trailer.Trail')),
            ],
        ),
        migrations.DeleteModel(
            name='PageView',
        ),
    ]
