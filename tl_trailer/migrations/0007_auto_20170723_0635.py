# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tl_trailer', '0006_auto_20170722_0430'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='expiry_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='group',
            name='notes',
            field=models.CharField(null=True, max_length=1024),
        ),
        migrations.AddField(
            model_name='group',
            name='owner',
            field=models.ForeignKey(null=True, to='tl_trailer.Owner', on_delete=django.db.models.deletion.SET_NULL),
        ),
    ]
