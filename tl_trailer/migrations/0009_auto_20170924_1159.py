# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import tl_trailer.models


class Migration(migrations.Migration):

    dependencies = [
        ('tl_trailer', '0008_auto_20170924_1156'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.IntegerField(default=tl_trailer.models._gen_id, primary_key=True, serialize=False)),
                ('expiry_date', models.DateField(null=True)),
                ('name', models.CharField(max_length=1024)),
                ('notes', models.CharField(max_length=1024, null=True)),
                ('latitude_min', models.FloatField(default=-90.0)),
                ('longitude_min', models.FloatField(default=-180.0)),
                ('latitude_max', models.FloatField(default=90.0)),
                ('longitude_max', models.FloatField(default=180.0)),
            ],
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.IntegerField(default=tl_trailer.models._gen_id, primary_key=True, serialize=False)),
                ('display_name', models.CharField(max_length=1024)),
            ],
        ),
        migrations.CreateModel(
            name='Trail',
            fields=[
                ('id', models.IntegerField(default=tl_trailer.models._gen_id, primary_key=True, serialize=False)),
                ('expiry_date', models.DateField(null=True)),
                ('locality', models.CharField(max_length=1024)),
                ('description', models.CharField(max_length=1024)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='tl_trailer.Owner', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.IntegerField(default=tl_trailer.models._gen_id, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=1024)),
                ('address', models.CharField(max_length=1024)),
                ('url', models.CharField(max_length=1024)),
                ('description', models.CharField(max_length=1024)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('radius_metres', models.FloatField()),
                ('southern_latitude', models.FloatField()),
                ('western_longitude', models.FloatField()),
                ('northern_latitude', models.FloatField()),
                ('eastern_longitude', models.FloatField()),
                ('trail', models.ForeignKey(to='tl_trailer.Trail')),
            ],
        ),
        migrations.AddField(
            model_name='group',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='tl_trailer.Owner', null=True),
        ),
        migrations.AddField(
            model_name='group',
            name='subgroups',
            field=models.ManyToManyField(to='tl_trailer.Group'),
        ),
        migrations.AddField(
            model_name='group',
            name='trails',
            field=models.ManyToManyField(to='tl_trailer.Trail'),
        ),
    ]
