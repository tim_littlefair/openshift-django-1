# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tl_trailer', '0005_auto_20170721_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='subgroups',
            field=models.ManyToManyField(to='tl_trailer.Group'),
        ),
    ]
