# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tl_trailer', '0007_auto_20170723_0635'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='group',
            name='subgroups',
        ),
        migrations.RemoveField(
            model_name='group',
            name='trails',
        ),
        migrations.RemoveField(
            model_name='trail',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='zone',
            name='trail',
        ),
        migrations.DeleteModel(
            name='Group',
        ),
        migrations.DeleteModel(
            name='Owner',
        ),
        migrations.DeleteModel(
            name='Trail',
        ),
        migrations.DeleteModel(
            name='Zone',
        ),
    ]
