
class Director:
    def __init__(self, builder):
        self.builder = builder

    def construct(self, locality_result, keyword_results, max_results):
        self.builder.construct_trail(locality_result)
        self.builder.add_zones(keyword_results, max_results)
        return self.builder.get_trail()
