import json
import statistics

from tl_trailer.models import Trail, Zone, save_with_unique_id


# The PyPi haversine library provides a function for calculating
# the distance between two pairs of GPS coordinates in km or miles.
# we want this distance in metres.
def haversine_metres(point1, point2):
    from haversine import haversine
    return haversine(point1, point2) * 1000.0


def _place_id(place_result):
    return place_result['place_id']


class DefaultBuilder:
    def __init__(self, min_radius_metres):
        self.trail = None
        self.min_radius_metres = min_radius_metres

    def construct_trail(self, locality_result_json):
        locality_result = json.loads(locality_result_json)
        self.trail = Trail()
        # print(locality_result)#['locality']
        self.trail.locality = locality_result['results'][0]['name']
        self.trail.description = ''
        save_with_unique_id(self.trail)

    def add_zones(self, keyword_results_json, max_results):
        keyword_results = json.loads(keyword_results_json)
        selected_results, singleton_results, multiple_results = [], [], []
        for keyword in keyword_results.keys():
            results = keyword_results[keyword]['results']
            results = DefaultBuilder._filter_results(results, keyword)
            if len(results) == 1:
                singleton_results += results
            else:
                multiple_results += [results, ]
        # If the filter only returns a single result for a keyword
        # we save it unconditionally.
        # If the number of keywords exceeds max_results, we assume
        # that the user is happy to receive a minimum of one result
        # per keyword.
        for result in singleton_results:
            selected_results += [result]
        # For keywords which return multiple results, we select results
        # in turn from each keywords list until the required number of
        # results has been reached (or until the lists run out)
        while len(selected_results) < max_results:
            if len(multiple_results) == 0:
                break
            head = multiple_results.pop()
            if len(head) == 0:
                continue
            selected_results.append(head[0])
            head = head[1:]  # pop?
            multiple_results.append(head)
        selected_results.sort(key=_place_id)
        for result in selected_results:
            self._add_zone(result)

    def get_trail(self):
        retval = self.trail
        self.trail = None
        return retval

    @staticmethod
    def _filter_results(results, keyword):
        for result in results:
            if (
                result['name'].startswith(keyword) or
                result['vicinity'].startswith(keyword)
            ):
                return [result, ]
        return results

    def _add_zone(self, result):
        new_zone = Zone()
        new_zone.trail = self.trail
        new_zone.name = result['name']
        new_zone.address = result['vicinity']
        new_zone.url = ''
        new_zone.description = 'Google placeid=%s' % (result['place_id'], )
        self._process_geometry(result['geometry'], new_zone)
        save_with_unique_id(new_zone)

    def _process_geometry(self, result_geometry, new_zone):
        viewport = result_geometry['viewport']
        viewport_diameter = haversine_metres(
            (viewport['southwest']['lat'], viewport['southwest']['lng']),
            (viewport['northeast']['lat'], viewport['northeast']['lng']),
        )
        # The geometry returned by Google Maps returns a point location
        # and a viewport (i.e. a projective 'rectangle' encompassing the
        # place and some surrounding territory).
        # The viewport diagonal never seems to be smaller than about 343
        # metres, and seems to be about a factor of 1.5-2 2 larger than
        # diagonal of the target area if it is more than this minimum
        # figure.
        VWPT_SLACK_FACTOR = 1.7
        if (viewport_diameter/VWPT_SLACK_FACTOR < self.min_radius_metres*2.0):
            new_zone.latitude = result_geometry['location']['lat']
            new_zone.longitude = result_geometry['location']['lng']
            new_zone.radius_metres = self.min_radius_metres
        else:
            new_zone.latitude = statistics.mean(
                (viewport['southwest']['lat'], viewport['northeast']['lat'],)
            )
            new_zone.longitude = statistics.mean(
                (viewport['southwest']['lng'], viewport['northeast']['lng'],)
            )
            new_zone.radius_metres = viewport_diameter/(2.0*VWPT_SLACK_FACTOR)
        # Even if we considered the viewport too large for the purpose
        # of calculating the radius of the zone, we still use it for the
        # bounds of the area to be displayed.
        new_zone.southern_latitude = viewport['southwest']['lat']
        new_zone.western_longitude = viewport['southwest']['lng']
        new_zone.northern_latitude = viewport['northeast']['lat']
        new_zone.eastern_longitude = viewport['northeast']['lng']
