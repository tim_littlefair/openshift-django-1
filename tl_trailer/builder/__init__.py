# This package will implement the GOF Builder Pattern
# in order to accept JSON Google Places search results
# for a locality and for a set of keywords associated
# with that locality in order to build a
# Django model of a trail centred on that locality
# with points for selected results returned by the
# API.

from .director import Director
from .builder import DefaultBuilder
