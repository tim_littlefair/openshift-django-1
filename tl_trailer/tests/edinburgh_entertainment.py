# Generated Sat Jul 22 08:10:23 2017
# pylint:disable=line-too-long,too-many-lines,missing-docstring

from tl_trailer.utilities.tuple_types import ExampleSearchData

edinburgh_entertainment = ExampleSearchData(
    'Edinburgh, UK',
    'Theatre\\nCinema\\nLive Music',
    {
        'locality_result':

        # https://maps.googleapis.com/maps/api/place/textsearch/json?key=_redacted_&query=Edinburgh%2C+UK
        """
{
  "html_attributions": [],
  "results": [
    {
      "formatted_address": "Edinburgh, UK",
      "geometry": {
        "location": {
          "lat": 55.953252,
          "lng": -3.188266999999999
        },
        "viewport": {
          "northeast": {
            "lat": 55.9917083,
            "lng": -3.0777483
          },
          "southwest": {
            "lat": 55.8904228,
            "lng": -3.3330187
          }
        }
      },
      "name": "Edinburgh",
      "photos": [
        {
          "height": 1152,
          "html_attributions": [
            "<a href=\\"https://maps.google.com/maps/contrib/117596503094836820498/photos\\">YY C</a>"
          ],
          "width": 2048
        }
      ],
      "place_id": "ChIJIyaYpQC4h0gRJxfnfHsU8mQ",
      "types": [
        "locality",
        "political"
      ]
    }
  ],
  "status": "OK"
}
""",

        'keyword_results':

        # https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=_redacted_&location=55.9532520%2C-3.1882670&radius=25000&keyword=Live+Music
        """
{
  "Cinema": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 55.9141197,
            "lng": -3.2855172
          },
          "viewport": {
            "northeast": {
              "lat": 55.9154686802915,
              "lng": -3.284168219708498
            },
            "southwest": {
              "lat": 55.9127707197085,
              "lng": -3.286866180291502
            }
          }
        },
        "name": "Odeon Cinemas Ltd",
        "place_id": "ChIJac4W1KHGh0gRoZ6G88vFCBc",
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Westside Plaza, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9806936,
            "lng": -3.1777806
          },
          "viewport": {
            "northeast": {
              "lat": 55.9820425802915,
              "lng": -3.176431619708498
            },
            "southwest": {
              "lat": 55.9793446197085,
              "lng": -3.179129580291502
            }
          }
        },
        "name": "Vue Cinemas",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2340,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/103215448183589686980/photos\\">Miles Gould</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJb4NXlgC4h0gRyxWR52z0osw",
        "rating": 3.8,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Ocean Drive, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9562872,
            "lng": -3.1858152
          },
          "viewport": {
            "northeast": {
              "lat": 55.9576479802915,
              "lng": -3.184815069708498
            },
            "southwest": {
              "lat": 55.95495001970851,
              "lng": -3.187513030291501
            }
          }
        },
        "name": "Vue",
        "photos": [
          {
            "height": 534,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111919082640857797964/photos\\">Omni Centre</a>"
            ],
            "width": 800
          }
        ],
        "place_id": "ChIJYeDx_IvHh0gRl7kl5nwkvvY",
        "rating": 3.9,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Omni Leisure Building 11, 61 Leith Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9413673,
            "lng": -3.2179769
          },
          "viewport": {
            "northeast": {
              "lat": 55.9426266302915,
              "lng": -3.216361019708498
            },
            "southwest": {
              "lat": 55.9399286697085,
              "lng": -3.219058980291502
            }
          }
        },
        "name": "Cineworld Cinema - Edinburgh",
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115600234759554481803/photos\\">Muhammad Haider</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJXz7u36fHh0gR9jjCzXnAx8E",
        "rating": 4.2,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Fountain Park, 130/3 Dundee Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94569749999999,
            "lng": -3.2059887
          },
          "viewport": {
            "northeast": {
              "lat": 55.9470765302915,
              "lng": -3.204382819708498
            },
            "southwest": {
              "lat": 55.9443785697085,
              "lng": -3.207080780291502
            }
          }
        },
        "name": "Odeon Cinema Edinburgh",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107886715547006216229/photos\\">D Ma</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJ0dHJoJjHh0gR2OOU0z7-SUg",
        "rating": 4.1,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "118 Lothian Road, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.93081950000001,
            "lng": -3.2087808
          },
          "viewport": {
            "northeast": {
              "lat": 55.93221793029149,
              "lng": -3.207437269708498
            },
            "southwest": {
              "lat": 55.92951996970849,
              "lng": -3.210135230291502
            }
          }
        },
        "name": "Dominion Cinema Edinburgh",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/118438298566467668651/photos\\">Becky Mingard</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJu2pZTgzHh0gRNhvXAzENjEA",
        "rating": 4.7,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Newbattle Terrace, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9151682,
            "lng": -3.2860976
          },
          "viewport": {
            "northeast": {
              "lat": 55.9165282302915,
              "lng": -3.285224869708498
            },
            "southwest": {
              "lat": 55.91383026970851,
              "lng": -3.287922830291501
            }
          }
        },
        "name": "ODEON Cinema",
        "photos": [
          {
            "height": 3120,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110193969471441620038/photos\\">Dan Salter</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJLQvPhZ_Gh0gRfCvUPXXFpUU",
        "rating": 4.3,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "120 Wester Hailes Road, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 56.0781364,
            "lng": -3.3949523
          },
          "viewport": {
            "northeast": {
              "lat": 56.07948538029149,
              "lng": -3.393603319708498
            },
            "southwest": {
              "lat": 56.0767874197085,
              "lng": -3.396301280291502
            }
          }
        },
        "name": "Odeon Cinema",
        "photos": [
          {
            "height": 1731,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/117571261024351350581/photos\\">Graeme Robertson</a>"
            ],
            "width": 2826
          }
        ],
        "place_id": "ChIJ1cR6Z93Nh0gRkjgGNe7lBjk",
        "rating": 3.8,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "1 Whimbrell Pl, Dunfermline"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94283100000001,
            "lng": -3.20406
          },
          "viewport": {
            "northeast": {
              "lat": 55.9441680802915,
              "lng": -3.202487769708498
            },
            "southwest": {
              "lat": 55.9414701197085,
              "lng": -3.205185730291502
            }
          }
        },
        "name": "Cameo Cinema",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2336,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/117974166247252867827/photos\\">Luna Nerea Carmona</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJhcSUP5_Hh0gRyRt-WvSr9yY",
        "rating": 4.6,
        "types": [
          "movie_theater",
          "bar",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "38 Home Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.88279980000001,
            "lng": -3.5199436
          },
          "viewport": {
            "northeast": {
              "lat": 55.8841487802915,
              "lng": -3.518594619708498
            },
            "southwest": {
              "lat": 55.8814508197085,
              "lng": -3.521292580291501
            }
          }
        },
        "name": "Vue",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107469143567954589154/photos\\">Stewart Hardie</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJHVI6zAHch0gRPsu5fsw_Ifk",
        "rating": 3.6,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "McArthur Glen Designer Outlet, 1, Almondvale Avenue, Livingston"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9352179,
            "lng": -3.1053922
          },
          "viewport": {
            "northeast": {
              "lat": 55.9365668802915,
              "lng": -3.104043219708498
            },
            "southwest": {
              "lat": 55.9338689197085,
              "lng": -3.106741180291502
            }
          }
        },
        "name": "ODEON",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107886715547006216229/photos\\">D Ma</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJRYGH3AW5h0gRYwWvVThLEMk",
        "rating": 4.3,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Newcraighall Road, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.95625070000001,
            "lng": -3.1861161
          },
          "viewport": {
            "northeast": {
              "lat": 55.9574391802915,
              "lng": -3.184176019708498
            },
            "southwest": {
              "lat": 55.9547412197085,
              "lng": -3.186873980291502
            }
          }
        },
        "name": "Omni Centre",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1373,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113118633794655451402/photos\\">Maggie Wright</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJRxrf5IvHh0gRgcly-Y-AuAw",
        "rating": 4.1,
        "types": [
          "movie_theater",
          "parking",
          "bar",
          "lodging",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Greenside Row, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9464657,
            "lng": -3.2060594
          },
          "viewport": {
            "northeast": {
              "lat": 55.9478324802915,
              "lng": -3.204550769708498
            },
            "southwest": {
              "lat": 55.9451345197085,
              "lng": -3.207248730291502
            }
          }
        },
        "name": "Filmhouse",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2610,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/116000842067878679657/photos\\">Dunkan Armstrong</a>"
            ],
            "width": 4640
          }
        ],
        "place_id": "ChIJ7ZTleaLHh0gRdYW_pjKxQDs",
        "rating": 4.5,
        "types": [
          "movie_theater",
          "cafe",
          "bar",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "88 Lothian Road, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.812923,
            "lng": -3.08867
          },
          "viewport": {
            "northeast": {
              "lat": 55.8142719802915,
              "lng": -3.087321019708498
            },
            "southwest": {
              "lat": 55.8115740197085,
              "lng": -3.090018980291501
            }
          }
        },
        "name": "Moorflix",
        "photos": [
          {
            "height": 2268,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/109056514803715322650/photos\\">Marina Dupliy</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJC3rNpyi-h0gR4nWOM1-TfRQ",
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Main Street, Temple"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.8843933,
            "lng": -3.3402377
          },
          "viewport": {
            "northeast": {
              "lat": 55.8858017802915,
              "lng": -3.338792869708498
            },
            "southwest": {
              "lat": 55.8831038197085,
              "lng": -3.341490830291501
            }
          }
        },
        "name": "Balerno Village Screen",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "place_id": "ChIJrRynzYPDh0gRNS7Spsu95Dk",
        "rating": 4.5,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "4 Main Street, Balerno"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.973416,
            "lng": -3.172523
          },
          "viewport": {
            "northeast": {
              "lat": 55.9747649802915,
              "lng": -3.171174019708498
            },
            "southwest": {
              "lat": 55.9720670197085,
              "lng": -3.173871980291502
            }
          }
        },
        "name": "INDY Cinema Group",
        "photos": [
          {
            "height": 3632,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/102389172848198838855/photos\\">INDY Cinema Group</a>"
            ],
            "width": 5456
          }
        ],
        "place_id": "ChIJB1WXJ_LHh0gRHB6j5M_KY4A",
        "types": [
          "electronics_store",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "89 Giles Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9588638,
            "lng": -3.201726
          },
          "viewport": {
            "northeast": {
              "lat": 55.96029998029149,
              "lng": -3.200426669708498
            },
            "southwest": {
              "lat": 55.9576020197085,
              "lng": -3.203124630291502
            }
          }
        },
        "name": "Edinburgh Cine & Video Society",
        "place_id": "ChIJP06Dj5THh0gRparDwNmz39Y",
        "rating": 5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "23A Fettes Row, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9536212,
            "lng": -3.1130363
          },
          "viewport": {
            "northeast": {
              "lat": 55.95493208029151,
              "lng": -3.111627119708498
            },
            "southwest": {
              "lat": 55.95223411970851,
              "lng": -3.114325080291501
            }
          }
        },
        "name": "George Bingo & Social Club",
        "place_id": "ChIJV9-LF7K5h0gR7OaNjPC7tfk",
        "rating": 5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "14 Bath Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9539699,
            "lng": -3.1989905
          },
          "viewport": {
            "northeast": {
              "lat": 55.9552591802915,
              "lng": -3.197590919708498
            },
            "southwest": {
              "lat": 55.9525612197085,
              "lng": -3.200288880291501
            }
          }
        },
        "name": "Home Control Scotland",
        "photos": [
          {
            "height": 1536,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115978390170331548603/photos\\">Home Control Scotland</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJB5QdNJHHh0gRtRto01sLvLY",
        "types": [
          "electronics_store",
          "home_goods_store",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "44 Thistle Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9578748,
            "lng": -3.2005953
          },
          "viewport": {
            "northeast": {
              "lat": 55.9591195302915,
              "lng": -3.199191319708498
            },
            "southwest": {
              "lat": 55.9564215697085,
              "lng": -3.201889280291501
            }
          }
        },
        "name": "CinemaAttic",
        "place_id": "ChIJEaw6epTHh0gRFoThh6AmG9k",
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "54A Great King Street, Edinburgh"
      }
    ]
  },
  "Live Music": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 55.9499513,
            "lng": -3.187218
          },
          "viewport": {
            "northeast": {
              "lat": 55.9513020302915,
              "lng": -3.185785069708498
            },
            "southwest": {
              "lat": 55.9486040697085,
              "lng": -3.188483030291502
            }
          }
        },
        "name": "Whistlebinkies Live Music Bar",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1520,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/102412429854221811402/photos\\">Frans Bongers</a>"
            ],
            "width": 2688
          }
        ],
        "place_id": "ChIJT_Z6hYXHh0gRAbsQZMud3Ew",
        "rating": 4.2,
        "types": [
          "bar",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "4-6 South Bridge, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94848449999999,
            "lng": -3.1878145
          },
          "viewport": {
            "northeast": {
              "lat": 55.9499134802915,
              "lng": -3.186482969708499
            },
            "southwest": {
              "lat": 55.9472155197085,
              "lng": -3.189180930291502
            }
          }
        },
        "name": "Stramash Live Music Bar",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 546,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/108335868847776924761/photos\\">Stramash Live Music Bar</a>"
            ],
            "width": 540
          }
        ],
        "place_id": "ChIJk_3_poXHh0gRn-1h7FKFJ0Y",
        "rating": 4.4,
        "types": [
          "bar",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "207 Cowgate, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9429402,
            "lng": -3.2086392
          },
          "viewport": {
            "northeast": {
              "lat": 55.9444798302915,
              "lng": -3.207395269708498
            },
            "southwest": {
              "lat": 55.9417818697085,
              "lng": -3.210093230291502
            }
          }
        },
        "name": "Badabing Edinburgh",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105061446959514181644/photos\\">Kenny Hodge</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJOZAEmaHHh0gR4mXZuI7XHXM",
        "rating": 4.2,
        "types": [
          "bar",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "2 Fountainbridge, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94804529999999,
            "lng": -3.1861646
          },
          "viewport": {
            "northeast": {
              "lat": 55.9493622302915,
              "lng": -3.184798519708498
            },
            "southwest": {
              "lat": 55.9466642697085,
              "lng": -3.187496480291502
            }
          }
        },
        "name": "The Royal Oak",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2448,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/103168645140666800899/photos\\">Adri\\u00e1n Burioni</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJSy0AyoXHh0gRQ5IjxMXshJQ",
        "rating": 4.3,
        "types": [
          "bar",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "1 Infirmary Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.946084,
            "lng": -3.191279999999999
          },
          "viewport": {
            "northeast": {
              "lat": 55.9474258302915,
              "lng": -3.189817819708498
            },
            "southwest": {
              "lat": 55.9447278697085,
              "lng": -3.192515780291502
            }
          }
        },
        "name": "Sandy Bell's",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2448,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110353687629862232406/photos\\">Martin Docherty</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJEUsUtYTHh0gRUoHMXAw1Sfs",
        "rating": 4.6,
        "types": [
          "bar",
          "cafe",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "25 Forrest Road, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.95381140000001,
            "lng": -3.190693
          },
          "viewport": {
            "northeast": {
              "lat": 55.9551195302915,
              "lng": -3.189339019708498
            },
            "southwest": {
              "lat": 55.95242156970851,
              "lng": -3.192036980291502
            }
          }
        },
        "name": "The Voodoo Rooms",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 800,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/109679089322175523941/photos\\">The Voodoo Rooms</a>"
            ],
            "width": 1200
          }
        ],
        "place_id": "ChIJgY3HN47Hh0gRAIzV-DIJewM",
        "rating": 4.4,
        "types": [
          "bar",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "19a West Register Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9533795,
            "lng": -3.1959504
          },
          "viewport": {
            "northeast": {
              "lat": 55.95487093029149,
              "lng": -3.194682219708498
            },
            "southwest": {
              "lat": 55.9521729697085,
              "lng": -3.197380180291502
            }
          }
        },
        "name": "Dirty Martini",
        "photos": [
          {
            "height": 683,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/109343976817698066663/photos\\">Dirty Martini</a>"
            ],
            "width": 1024
          }
        ],
        "place_id": "ChIJr1_CB5HHh0gRgYuiHKdHk4E",
        "rating": 4,
        "types": [
          "bar",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "16 George Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9498114,
            "lng": -3.2080447
          },
          "viewport": {
            "northeast": {
              "lat": 55.9511120302915,
              "lng": -3.206572969708497
            },
            "southwest": {
              "lat": 55.9484140697085,
              "lng": -3.209270930291501
            }
          }
        },
        "name": "Ghillie Dhu",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1075,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105622530328780985120/photos\\">Ghillie Dhu</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJoxdWS73Hh0gRDjh0bSOsa7Q",
        "rating": 4.1,
        "types": [
          "bar",
          "movie_theater",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "2 Rutland Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9577986,
            "lng": -3.2128078
          },
          "viewport": {
            "northeast": {
              "lat": 55.9592004302915,
              "lng": -3.211509669708498
            },
            "southwest": {
              "lat": 55.9565024697085,
              "lng": -3.214207630291502
            }
          }
        },
        "name": "The Raeburn Bar",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 718,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107920231156635362766/photos\\">The Raeburn Bar</a>"
            ],
            "width": 718
          }
        ],
        "place_id": "ChIJuSCO4L7Hh0gRlmMgba5nVLQ",
        "rating": 4,
        "types": [
          "bar",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "50 Dean Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9479814,
            "lng": -3.1949222
          },
          "viewport": {
            "northeast": {
              "lat": 55.9492828302915,
              "lng": -3.193536769708498
            },
            "southwest": {
              "lat": 55.9465848697085,
              "lng": -3.196234730291502
            }
          }
        },
        "name": "Biddy Mulligans",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 665,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111105493037564660427/photos\\">G1 Group</a>"
            ],
            "width": 1000
          }
        ],
        "place_id": "ChIJO8GUVprHh0gRCwIU9dKXbyo",
        "rating": 4,
        "types": [
          "bar",
          "night_club",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "94-96 Grassmarket, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94809129999999,
            "lng": -3.1869932
          },
          "viewport": {
            "northeast": {
              "lat": 55.9493652802915,
              "lng": -3.185606019708498
            },
            "southwest": {
              "lat": 55.9466673197085,
              "lng": -3.188303980291502
            }
          }
        },
        "name": "The Jazz Bar",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/103211522302693244471/photos\\">Isu Jusi</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJl-7ssYXHh0gRNGpPGtAkx-w",
        "rating": 4.5,
        "types": [
          "bar",
          "night_club",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "1A Chambers Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94883799999999,
            "lng": -3.186601099999999
          },
          "viewport": {
            "northeast": {
              "lat": 55.9501614302915,
              "lng": -3.185239719708499
            },
            "southwest": {
              "lat": 55.9474634697085,
              "lng": -3.187937680291502
            }
          }
        },
        "name": "Bannerman's Bar",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101530789853756232359/photos\\">Roger Smith</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJS1AcvIXHh0gRunRUW1qXLBs",
        "rating": 4.4,
        "types": [
          "bar",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "212 Cowgate, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.948595,
            "lng": -3.19354
          },
          "viewport": {
            "northeast": {
              "lat": 55.9499974302915,
              "lng": -3.192214719708498
            },
            "southwest": {
              "lat": 55.9472994697085,
              "lng": -3.194912680291501
            }
          }
        },
        "name": "Finnegan's Wake",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1360,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/108146617884558068467/photos\\">Finnegan&#39;s Wake</a>"
            ],
            "width": 1363
          }
        ],
        "place_id": "ChIJs9Y06prHh0gRpcd6OxDNwP8",
        "rating": 4.1,
        "types": [
          "bar",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "9 Victoria Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9498976,
            "lng": -3.2084037
          },
          "viewport": {
            "northeast": {
              "lat": 55.9512897802915,
              "lng": -3.207144769708498
            },
            "southwest": {
              "lat": 55.9485918197085,
              "lng": -3.209842730291502
            }
          }
        },
        "name": "The Rat Pack Piano Bar",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2160,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110621820227622106368/photos\\">Susan Lorimer</a>"
            ],
            "width": 3840
          }
        ],
        "place_id": "ChIJSQI_zKLHh0gRr9x1RS6E53c",
        "rating": 3.9,
        "types": [
          "bar",
          "night_club",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "9 Shandwick Place, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9482278,
            "lng": -3.1915123
          },
          "viewport": {
            "northeast": {
              "lat": 55.9495977302915,
              "lng": -3.190175819708498
            },
            "southwest": {
              "lat": 55.9468997697085,
              "lng": -3.192873780291502
            }
          }
        },
        "name": "Sneaky Pete's",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114618352823594843038/photos\\">\\u062b\\u0627\\u0645\\u0631 \\u0627\\u0644\\u063a\\u0627\\u0644\\u064a</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJabBvLYXHh0gRB6Ud2emzpuo",
        "rating": 4,
        "types": [
          "night_club",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "73 Cowgate, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.952655,
            "lng": -3.1917389
          },
          "viewport": {
            "northeast": {
              "lat": 55.9540039802915,
              "lng": -3.190389919708498
            },
            "southwest": {
              "lat": 55.9513060197085,
              "lng": -3.193087880291502
            }
          }
        },
        "name": "Malones on the Mall",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2398,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/112480407527284152182/photos\\">Malones on the Mall</a>"
            ],
            "width": 3412
          }
        ],
        "place_id": "ChIJNUxOsYTHh0gR3e8vQehgAcg",
        "rating": 4.1,
        "types": [
          "bar",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Waverley Mall Rooftop, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9488645,
            "lng": -3.187335399999999
          },
          "viewport": {
            "northeast": {
              "lat": 55.9504126302915,
              "lng": -3.186232119708498
            },
            "southwest": {
              "lat": 55.9477146697085,
              "lng": -3.188930080291501
            }
          }
        },
        "name": "Cabaret Voltaire",
        "photos": [
          {
            "height": 960,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106187428398794031684/photos\\">Scott Liddell</a>"
            ],
            "width": 1440
          }
        ],
        "place_id": "ChIJ8-Zqn4XHh0gR0OKyoEhxvLE",
        "rating": 3.9,
        "types": [
          "night_club",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "36-38 Blair Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.95353249999999,
            "lng": -3.1963523
          },
          "viewport": {
            "northeast": {
              "lat": 55.9549269802915,
              "lng": -3.194981119708498
            },
            "southwest": {
              "lat": 55.9522290197085,
              "lng": -3.197679080291501
            }
          }
        },
        "name": "Hard Rock Cafe",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1195,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115098767468883332708/photos\\">Hard Rock Cafe</a>"
            ],
            "width": 1197
          }
        ],
        "place_id": "ChIJsbrmBZHHh0gRzJntEk_0L50",
        "rating": 4.3,
        "types": [
          "restaurant",
          "bar",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "20 George Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.945139,
            "lng": -3.199759
          },
          "viewport": {
            "northeast": {
              "lat": 55.9463352302915,
              "lng": -3.198321069708498
            },
            "southwest": {
              "lat": 55.9436372697085,
              "lng": -3.201019030291502
            }
          }
        },
        "name": "The Tap - Music Lounge",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 895,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114469054672851185913/photos\\">The Tap - Music Lounge</a>"
            ],
            "width": 960
          }
        ],
        "place_id": "ChIJQdpsT5nHh0gRQu1Gsf2XwOM",
        "types": [
          "bar",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "80 Lauriston Place, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9683874,
            "lng": -3.1740061
          },
          "viewport": {
            "northeast": {
              "lat": 55.9697005302915,
              "lng": -3.172524669708498
            },
            "southwest": {
              "lat": 55.9670025697085,
              "lng": -3.175222630291501
            }
          }
        },
        "name": "Leith Depot",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2349,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115573639536924460902/photos\\">Alan Paul</a>"
            ],
            "width": 3394
          }
        ],
        "place_id": "ChIJC3nWaQm4h0gROVMK7Y-EFn0",
        "rating": 4.3,
        "types": [
          "restaurant",
          "bar",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "140 Leith Walk, Edinburgh"
      }
    ]
  },
  "Theatre": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 55.9522851,
            "lng": -3.2024999
          },
          "viewport": {
            "northeast": {
              "lat": 55.9537712802915,
              "lng": -3.201224219708498
            },
            "southwest": {
              "lat": 55.95107331970851,
              "lng": -3.203922180291502
            }
          }
        },
        "name": "New Town Theatre",
        "place_id": "ChIJxwbLBZfHh0gR33MBJxBtFwM",
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Freemasons Hall, 96 George Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.946268,
            "lng": -3.190704
          },
          "viewport": {
            "northeast": {
              "lat": 55.94767358029149,
              "lng": -3.189270369708498
            },
            "southwest": {
              "lat": 55.94497561970849,
              "lng": -3.191968330291501
            }
          }
        },
        "name": "Bedlam Theatre",
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113330374472138617322/photos\\">Kevin Mowat</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJD-O1yYTHh0gR4YPjQN5zrUI",
        "rating": 4.6,
        "types": [
          "university",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "11 Bristo Place, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94683200000001,
            "lng": -3.2044301
          },
          "viewport": {
            "northeast": {
              "lat": 55.9481809802915,
              "lng": -3.203081119708498
            },
            "southwest": {
              "lat": 55.9454830197085,
              "lng": -3.205779080291502
            }
          }
        },
        "name": "The Lyceum",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1345,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113305272903087166082/photos\\">The Lyceum</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJYUp8i5jHh0gRbeekyA8OJWk",
        "rating": 4.5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Grindlay Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 56.0701012,
            "lng": -3.4581027
          },
          "viewport": {
            "northeast": {
              "lat": 56.07149508029149,
              "lng": -3.456585269708499
            },
            "southwest": {
              "lat": 56.06879711970849,
              "lng": -3.459283230291502
            }
          }
        },
        "name": "Alhambra Theatre",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2935,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114811331899673609950/photos\\">Alhambra Theatre</a>"
            ],
            "width": 4934
          }
        ],
        "place_id": "ChIJDSlFliHSh0gRSfqOxGpKbAE",
        "rating": 4.2,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "33-35 Canmore Street, Dunfermline"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9562872,
            "lng": -3.1858152
          },
          "viewport": {
            "northeast": {
              "lat": 55.9576479802915,
              "lng": -3.184815069708498
            },
            "southwest": {
              "lat": 55.95495001970851,
              "lng": -3.187513030291501
            }
          }
        },
        "name": "Vue",
        "photos": [
          {
            "height": 534,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111919082640857797964/photos\\">Omni Centre</a>"
            ],
            "width": 800
          }
        ],
        "place_id": "ChIJYeDx_IvHh0gRl7kl5nwkvvY",
        "rating": 3.9,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Omni Leisure Building 11, 61 Leith Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9413673,
            "lng": -3.2179769
          },
          "viewport": {
            "northeast": {
              "lat": 55.9426266302915,
              "lng": -3.216361019708498
            },
            "southwest": {
              "lat": 55.9399286697085,
              "lng": -3.219058980291502
            }
          }
        },
        "name": "Cineworld Cinema - Edinburgh",
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115600234759554481803/photos\\">Muhammad Haider</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJXz7u36fHh0gR9jjCzXnAx8E",
        "rating": 4.2,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Fountain Park, 130/3 Dundee Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 56.1120241,
            "lng": -3.1636848
          },
          "viewport": {
            "northeast": {
              "lat": 56.1133116302915,
              "lng": -3.162745219708498
            },
            "southwest": {
              "lat": 56.1106136697085,
              "lng": -3.165443180291502
            }
          }
        },
        "name": "Adam Smith Theatre",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1361,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/103270684537362267157/photos\\">Adam Smith Theatre</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJtyCkM6K1h0gRCQsdRqwAn2E",
        "rating": 4.5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Bennochy Road, Kirkcaldy"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.943788,
            "lng": -3.057014
          },
          "viewport": {
            "northeast": {
              "lat": 55.94510898029149,
              "lng": -3.055595669708498
            },
            "southwest": {
              "lat": 55.94241101970849,
              "lng": -3.058293630291502
            }
          }
        },
        "name": "The Brunton, Musselburgh",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 550,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106046739126976362868/photos\\">The Brunton, Musselburgh</a>"
            ],
            "width": 872
          }
        ],
        "place_id": "ChIJOe1z3Sq6h0gRY9TR6QQYW4s",
        "rating": 4.3,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Ladywell Way, Musselburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.93081950000001,
            "lng": -3.2087808
          },
          "viewport": {
            "northeast": {
              "lat": 55.93221793029149,
              "lng": -3.207437269708498
            },
            "southwest": {
              "lat": 55.92951996970849,
              "lng": -3.210135230291502
            }
          }
        },
        "name": "Dominion Cinema Edinburgh",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/118438298566467668651/photos\\">Becky Mingard</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJu2pZTgzHh0gRNhvXAzENjEA",
        "rating": 4.7,
        "types": [
          "movie_theater",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Newbattle Terrace, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9572047,
            "lng": -3.185040900000001
          },
          "viewport": {
            "northeast": {
              "lat": 55.95859778029151,
              "lng": -3.183818319708499
            },
            "southwest": {
              "lat": 55.95589981970851,
              "lng": -3.186516280291503
            }
          }
        },
        "name": "The Theatre Royal Bar",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2448,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/103775784276787246930/photos\\">Pheonix Property</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJ0Te59ovHh0gRu-TEcm3o-8E",
        "rating": 4,
        "types": [
          "bar",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "25-27 Greenside Lane, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94283100000001,
            "lng": -3.20406
          },
          "viewport": {
            "northeast": {
              "lat": 55.9441680802915,
              "lng": -3.202487769708498
            },
            "southwest": {
              "lat": 55.9414701197085,
              "lng": -3.205185730291502
            }
          }
        },
        "name": "Cameo Cinema",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2336,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/117974166247252867827/photos\\">Luna Nerea Carmona</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJhcSUP5_Hh0gRyRt-WvSr9yY",
        "rating": 4.6,
        "types": [
          "movie_theater",
          "bar",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "38 Home Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 56.1124458,
            "lng": -3.1558227
          },
          "viewport": {
            "northeast": {
              "lat": 56.1138090302915,
              "lng": -3.154626169708498
            },
            "southwest": {
              "lat": 56.1111110697085,
              "lng": -3.157324130291502
            }
          }
        },
        "name": "Kings Theatre",
        "place_id": "ChIJ1xry0aW1h0gRKmWASKUAPAI",
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "260 High Street, Kirkcaldy"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94664439999999,
            "lng": -3.2045659
          },
          "viewport": {
            "northeast": {
              "lat": 55.9480653802915,
              "lng": -3.203166169708498
            },
            "southwest": {
              "lat": 55.9453674197085,
              "lng": -3.205864130291502
            }
          }
        },
        "name": "Lung Ha's Theatre Company",
        "place_id": "ChIJIVgvj5jHh0gRoIP4UVuUBlQ",
        "rating": 5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "30 Grindlay Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.94193060000001,
            "lng": -3.2032829
          },
          "viewport": {
            "northeast": {
              "lat": 55.94328203029151,
              "lng": -3.202007169708498
            },
            "southwest": {
              "lat": 55.94058406970851,
              "lng": -3.204705130291502
            }
          }
        },
        "name": "King's Theatre",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2160,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/112394782760001511948/photos\\">Mister Woo</a>"
            ],
            "width": 3840
          }
        ],
        "place_id": "ChIJa6hZCZ_Hh0gRqjiD9TMfcds",
        "rating": 4.6,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "2 Leven Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.947648,
            "lng": -3.2048889
          },
          "viewport": {
            "northeast": {
              "lat": 55.94904513029149,
              "lng": -3.203647269708498
            },
            "southwest": {
              "lat": 55.9463471697085,
              "lng": -3.206345230291502
            }
          }
        },
        "name": "Traverse Theatre",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2448,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113206174940258650181/photos\\">Bettina Fiedler</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJVUiaZJjHh0gRNXvjJzENf7Y",
        "rating": 4.5,
        "types": [
          "cafe",
          "bar",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "10 Cambridge Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.97566059999999,
            "lng": -3.180622199999999
          },
          "viewport": {
            "northeast": {
              "lat": 55.9767699302915,
              "lng": -3.179154019708498
            },
            "southwest": {
              "lat": 55.9740719697085,
              "lng": -3.181851980291502
            }
          }
        },
        "name": "Thomas Morton Hall",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/109478414833695194762/photos\\">Andy Meldrum</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJ6Yjje_jHh0gRK1hlyTXdHG0",
        "rating": 4.5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Leith Library, 28-30 Ferry Road, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9505263,
            "lng": -3.2000663
          },
          "viewport": {
            "northeast": {
              "lat": 55.9518078302915,
              "lng": -3.198629969708498
            },
            "southwest": {
              "lat": 55.9491098697085,
              "lng": -3.201327930291502
            }
          }
        },
        "name": "Ross Theatre",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101873698663026825150/photos\\">Manuel M\\u00e1rquez Chapresto</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJn71oOpDHh0gRIi84DjjibdM",
        "rating": 4,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Princes Street, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9352576,
            "lng": -3.1315885
          },
          "viewport": {
            "northeast": {
              "lat": 55.9363360302915,
              "lng": -3.130325019708498
            },
            "southwest": {
              "lat": 55.9336380697085,
              "lng": -3.133022980291502
            }
          }
        },
        "name": "Lyra Theatre",
        "place_id": "ChIJi0FsAJHHh0gRswdGjDzsOYM",
        "rating": 5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "11 Harewood Road, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9464657,
            "lng": -3.2060594
          },
          "viewport": {
            "northeast": {
              "lat": 55.9478324802915,
              "lng": -3.204550769708498
            },
            "southwest": {
              "lat": 55.9451345197085,
              "lng": -3.207248730291502
            }
          }
        },
        "name": "Filmhouse",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2610,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/116000842067878679657/photos\\">Dunkan Armstrong</a>"
            ],
            "width": 4640
          }
        ],
        "place_id": "ChIJ7ZTleaLHh0gRdYW_pjKxQDs",
        "rating": 4.5,
        "types": [
          "movie_theater",
          "cafe",
          "bar",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "88 Lothian Road, Edinburgh"
      },
      {
        "geometry": {
          "location": {
            "lat": 55.9649039,
            "lng": -3.2739531
          },
          "viewport": {
            "northeast": {
              "lat": 55.96625288029149,
              "lng": -3.272604119708498
            },
            "southwest": {
              "lat": 55.9635549197085,
              "lng": -3.275302080291502
            }
          }
        },
        "name": "Edinburgh Theatre Arts",
        "place_id": "ChIJzae6YbjHh0gRuANnlzLjVe4",
        "rating": 2,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "St Ninians Church Hall, Comely Bank Rd, Edinburgh"
      }
    ]
  }
}
""",

    }
)
