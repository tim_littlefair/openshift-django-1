# Generated Sat Jul 22 08:10:30 2017
# pylint:disable=line-too-long,too-many-lines,missing-docstring

from tl_trailer.utilities.tuple_types import ExampleSearchData

london_tourist_spots = ExampleSearchData(
    'London, UK',
    'Trafalgar Square\\nTower Bridge\\nKings Cross Station',
    {
        'locality_result':

        # https://maps.googleapis.com/maps/api/place/textsearch/json?key=_redacted_&query=London%2C+UK
        """
{
  "html_attributions": [],
  "results": [
    {
      "formatted_address": "London, UK",
      "geometry": {
        "location": {
          "lat": 51.5073509,
          "lng": -0.1277583
        },
        "viewport": {
          "northeast": {
            "lat": 51.6723432,
            "lng": 0.1482319
          },
          "southwest": {
            "lat": 51.38494009999999,
            "lng": -0.3514683
          }
        }
      },
      "name": "London",
      "photos": [
        {
          "height": 1080,
          "html_attributions": [
            "<a href=\\"https://maps.google.com/maps/contrib/104976517356691726555/photos\\">Petar Stoimenov</a>"
          ],
          "width": 1920
        }
      ],
      "place_id": "ChIJdd4hrwug2EcRmSrV3Vo6llI",
      "types": [
        "locality",
        "political"
      ]
    }
  ],
  "status": "OK"
}
""",

        'keyword_results':

        # https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=_redacted_&location=51.5073509%2C-0.1277583&radius=25000&keyword=Kings+Cross+Station
        """
{
  "Kings Cross Station": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 51.5316396,
            "lng": -0.1244231
          },
          "viewport": {
            "northeast": {
              "lat": 51.5329885802915,
              "lng": -0.123074119708498
            },
            "southwest": {
              "lat": 51.5302906197085,
              "lng": -0.125772080291502
            }
          }
        },
        "name": "King's Cross",
        "photos": [
          {
            "height": 2403,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106132370739132997978/photos\\">Dom Trinh</a>"
            ],
            "width": 3600
          }
        ],
        "place_id": "ChIJmxO_XDwbdkgR-zjbcc_J6Xs",
        "rating": 4.2,
        "types": [
          "train_station",
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Euston Road, London, Greater London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.531427,
            "lng": -0.126133
          },
          "viewport": {
            "northeast": {
              "lat": 51.53327890000001,
              "lng": -0.1240858500000001
            },
            "southwest": {
              "lat": 51.52805689999999,
              "lng": -0.1279100499999999
            }
          }
        },
        "name": "St Pancras International",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1065,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/117103378561356887789/photos\\">St Pancras International</a>"
            ],
            "width": 1600
          }
        ],
        "place_id": "ChIJJe2YjTsbdkgREt0yqM9vLbk",
        "price_level": 3,
        "rating": 4.3,
        "types": [
          "train_station",
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53215779999999,
            "lng": -0.1239774
          },
          "viewport": {
            "northeast": {
              "lat": 51.5335067802915,
              "lng": -0.122628419708498
            },
            "southwest": {
              "lat": 51.5308088197085,
              "lng": -0.125326380291502
            }
          }
        },
        "name": "Platform 9\\u00be",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 4608,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101110232987934925037/photos\\">Markus Steinemann</a>"
            ],
            "width": 2592
          }
        ],
        "place_id": "ChIJJe7ZEDwbdkgRCHZ9Qztth8w",
        "rating": 4.2,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Kings Cross Station, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53154079999999,
            "lng": -0.1252147
          },
          "viewport": {
            "northeast": {
              "lat": 51.5328897802915,
              "lng": -0.123865719708498
            },
            "southwest": {
              "lat": 51.5301918197085,
              "lng": -0.126563680291502
            }
          }
        },
        "name": "King's Cross St. Pancras",
        "photos": [
          {
            "height": 1613,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/116334626492603518138/photos\\">Miguel Meirinhos</a>"
            ],
            "width": 4959
          }
        ],
        "place_id": "ChIJoWUrbDwbdkgR12j3NK1BLC8",
        "rating": 5,
        "types": [
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "United Kingdom"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5304203,
            "lng": -0.1238148
          },
          "viewport": {
            "northeast": {
              "lat": 51.53166093029149,
              "lng": -0.122348969708498
            },
            "southwest": {
              "lat": 51.52896296970849,
              "lng": -0.125046930291502
            }
          }
        },
        "name": "King's Cross St. Pancras Underground Station",
        "photos": [
          {
            "height": 2304,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/116114169448165555401/photos\\">Permsak Tosawad</a>"
            ],
            "width": 3456
          }
        ],
        "place_id": "ChIJx4FA-TsbdkgR0KLHklorYXw",
        "rating": 4.3,
        "types": [
          "subway_station",
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53177069999999,
            "lng": -0.1247551
          },
          "viewport": {
            "northeast": {
              "lat": 51.53311968029148,
              "lng": -0.123406119708498
            },
            "southwest": {
              "lat": 51.53042171970849,
              "lng": -0.126104080291502
            }
          }
        },
        "name": "Boots",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2322,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/112177986963730746304/photos\\">Benj\\u00e1min Horv\\u00e1th</a>"
            ],
            "width": 4128
          }
        ],
        "place_id": "ChIJcWQqMDobdkgRsCl-l69bf9Y",
        "rating": 3.6,
        "types": [
          "pharmacy",
          "health",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53190780000001,
            "lng": -0.1265722
          },
          "viewport": {
            "northeast": {
              "lat": 51.53347128029151,
              "lng": -0.124830819708498
            },
            "southwest": {
              "lat": 51.53077331970851,
              "lng": -0.1275287802915021
            }
          }
        },
        "name": "British Transport Police",
        "place_id": "ChIJx7bHZnwRdkgRCuZHcJeJlRo",
        "rating": 5,
        "types": [
          "police",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "St Pancras International Railway Station, Pancras Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5313418,
            "lng": -0.1245543
          },
          "viewport": {
            "northeast": {
              "lat": 51.5326907802915,
              "lng": -0.123205319708498
            },
            "southwest": {
              "lat": 51.5299928197085,
              "lng": -0.125903280291502
            }
          }
        },
        "name": "Patisserie Valerie",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 4140,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113172558260066199417/photos\\">\\u0410\\u043d\\u0434\\u0440\\u0435\\u0439 \\u0411\\u0430\\u0443\\u044d\\u0440</a>"
            ],
            "width": 5520
          }
        ],
        "place_id": "ChIJRfFZyHtx30cRjkr8SXtnlDQ",
        "price_level": 3,
        "rating": 3.5,
        "types": [
          "restaurant",
          "cafe",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "M1, London King's Cross, Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53157950000001,
            "lng": -0.1246399
          },
          "viewport": {
            "northeast": {
              "lat": 51.5329284802915,
              "lng": -0.123290919708498
            },
            "southwest": {
              "lat": 51.5302305197085,
              "lng": -0.125988880291502
            }
          }
        },
        "name": "Pret A Manger",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2814,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105232639450015400868/photos\\">City and Village</a>"
            ],
            "width": 2300
          }
        ],
        "place_id": "ChIJSf66EzwbdkgRywNCHHXqk6Q",
        "price_level": 2,
        "rating": 4.1,
        "types": [
          "cafe",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "1 Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.533441,
            "lng": -0.12095
          },
          "viewport": {
            "northeast": {
              "lat": 51.5348504302915,
              "lng": -0.119601069708498
            },
            "southwest": {
              "lat": 51.5321524697085,
              "lng": -0.122299030291502
            }
          }
        },
        "name": "Central Station",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/109522827549301913507/photos\\">Tijen Mustafa</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJvU40Vj4bdkgRTd24XULHrKs",
        "rating": 4.1,
        "types": [
          "bar",
          "night_club",
          "lodging",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "37 Wharfdale Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5317712,
            "lng": -0.1238966
          },
          "viewport": {
            "northeast": {
              "lat": 51.5331201802915,
              "lng": -0.122547619708498
            },
            "southwest": {
              "lat": 51.53042221970851,
              "lng": -0.125245580291502
            }
          }
        },
        "name": "Virgin Trains East Coast First Class Lounge",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3088,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115140723469590379879/photos\\">Pablo Savva</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJu7YPGTwbdkgRyFOmIVJy1f4",
        "rating": 4.1,
        "types": [
          "bar",
          "night_club",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "London Kings Cross, Saint Pancras Way, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53180159999999,
            "lng": -0.1244779
          },
          "viewport": {
            "northeast": {
              "lat": 51.5331505802915,
              "lng": -0.123128919708498
            },
            "southwest": {
              "lat": 51.5304526197085,
              "lng": -0.125826880291502
            }
          }
        },
        "name": "WHSmith",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3264,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105232639450015400868/photos\\">City and Village</a>"
            ],
            "width": 2448
          }
        ],
        "place_id": "ChIJ4Yg44lQDdkgREGEvcigweeA",
        "price_level": 2,
        "rating": 3.4,
        "types": [
          "convenience_store",
          "book_store",
          "food",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "1 Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5300305,
            "lng": -0.1232722
          },
          "viewport": {
            "northeast": {
              "lat": 51.53146038029149,
              "lng": -0.122016219708498
            },
            "southwest": {
              "lat": 51.52876241970849,
              "lng": -0.124714180291502
            }
          }
        },
        "name": "Taxi Rank",
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113903990522062519921/photos\\">Eliot james</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJc3IkWDkbdkgRCOxRMKXkTKw",
        "rating": 3.8,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "13 Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53258899999999,
            "lng": -0.127224
          },
          "viewport": {
            "northeast": {
              "lat": 51.5339379802915,
              "lng": -0.125875019708498
            },
            "southwest": {
              "lat": 51.5312400197085,
              "lng": -0.128572980291502
            }
          }
        },
        "name": "Excess Baggage Company - Left Luggage",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1023,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/112605903884456738114/photos\\">Excess Baggage Company - Left Luggage</a>"
            ],
            "width": 1023
          }
        ],
        "place_id": "ChIJHY7cID0bdkgRqHHfBeh9N5k",
        "rating": 2.2,
        "types": [
          "storage",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Excess Baggage Company, Left Luggage / Left Baggage, Main Concourse, St Pancras International, Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5306187,
            "lng": -0.1262421
          },
          "viewport": {
            "northeast": {
              "lat": 51.5319676802915,
              "lng": -0.124893119708498
            },
            "southwest": {
              "lat": 51.5292697197085,
              "lng": -0.127591080291502
            }
          }
        },
        "name": "Le Pain Quotidien",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2448,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111146438532945651873/photos\\">Andrew Seftel</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJWTFOqDsbdkgRjLJu7VmP8f4",
        "rating": 3.3,
        "types": [
          "bakery",
          "restaurant",
          "food",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Unit 4, St Pancras International, Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5282291,
            "lng": -0.1291363
          },
          "viewport": {
            "northeast": {
              "lat": 51.5296549802915,
              "lng": -0.127603069708498
            },
            "southwest": {
              "lat": 51.5269570197085,
              "lng": -0.130301030291502
            }
          }
        },
        "name": "No1 Currency Exchange London, Kings Cross",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 470,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111750188007785959749/photos\\">No1 Currency Exchange London, Kings Cross</a>"
            ],
            "width": 470
          }
        ],
        "place_id": "ChIJTRETJzsbdkgRbGE1wuoe54Q",
        "rating": 2.3,
        "types": [
          "finance",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "128 Euston Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53174569999999,
            "lng": -0.1246435
          },
          "viewport": {
            "northeast": {
              "lat": 51.5330946802915,
              "lng": -0.123294519708498
            },
            "southwest": {
              "lat": 51.5303967197085,
              "lng": -0.125992480291502
            }
          }
        },
        "name": "Benito's Hat",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2160,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/100289057150326503097/photos\\">Jonathan CT Smith</a>"
            ],
            "width": 3840
          }
        ],
        "place_id": "ChIJu8rMqz4bdkgRTsrRtzLRJpc",
        "rating": 3.9,
        "types": [
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Kings Cross station, Pancras Rd, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5317526,
            "lng": -0.1240226
          },
          "viewport": {
            "northeast": {
              "lat": 51.53310158029149,
              "lng": -0.122673619708498
            },
            "southwest": {
              "lat": 51.53040361970849,
              "lng": -0.125371580291502
            }
          }
        },
        "name": "little Waitrose",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2268,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/117339755928990295438/photos\\">Liselotte Laurila</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJXeuFGTwbdkgRDvkPEFnOi9w",
        "price_level": 2,
        "rating": 3.8,
        "types": [
          "convenience_store",
          "grocery_or_supermarket",
          "food",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "4, Concourse, Kings Cross Station, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53106810000001,
            "lng": -0.12461
          },
          "viewport": {
            "northeast": {
              "lat": 51.5323684802915,
              "lng": -0.123413619708498
            },
            "southwest": {
              "lat": 51.5296705197085,
              "lng": -0.126111580291502
            }
          }
        },
        "name": "GNH Bar",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1164,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/116715534578175586940/photos\\">GNH Bar</a>"
            ],
            "width": 1164
          }
        ],
        "place_id": "ChIJnWJFCzwbdkgRjgv48nY_8ow",
        "rating": 4,
        "types": [
          "bar",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Kings Cross Station, Great Northern Hotel, Pancras Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.53127300000001,
            "lng": -0.12465
          },
          "viewport": {
            "northeast": {
              "lat": 51.5326219802915,
              "lng": -0.123301019708498
            },
            "southwest": {
              "lat": 51.5299240197085,
              "lng": -0.125998980291502
            }
          }
        },
        "name": "M&S Kings Cross Rail Simply Food",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2448,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111146438532945651873/photos\\">Andrew Seftel</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJLaC-Tj8bdkgR4Gs_nk_ftGU",
        "price_level": 2,
        "rating": 3.6,
        "types": [
          "convenience_store",
          "liquor_store",
          "grocery_or_supermarket",
          "florist",
          "food",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Kings Cross Br Station, London"
      }
    ]
  },
  "Tower Bridge": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 51.5054564,
            "lng": -0.07535649999999999
          },
          "viewport": {
            "northeast": {
              "lat": 51.51107135,
              "lng": -0.07267754999999998
            },
            "southwest": {
              "lat": 51.50151474999998,
              "lng": -0.07777535000000001
            }
          }
        },
        "name": "Tower Bridge",
        "photos": [
          {
            "height": 538,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/102704363979071108208/photos\\">Adam Stephenson</a>"
            ],
            "width": 800
          }
        ],
        "place_id": "ChIJSdtli0MDdkgRLW9aCBpCeJ4",
        "rating": 4.7,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Tower Bridge Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.503987,
            "lng": -0.07660299999999999
          },
          "viewport": {
            "northeast": {
              "lat": 51.5053127302915,
              "lng": -0.07516301970849797
            },
            "southwest": {
              "lat": 51.5026147697085,
              "lng": -0.07786098029150203
            }
          }
        },
        "name": "Tower Bridge Exhibition Offices",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3376,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/102262185520634609395/photos\\">Kimberly Chun</a>"
            ],
            "width": 6000
          }
        ],
        "place_id": "ChIJSdtli0MDdkgRrgRLMv8r0IQ",
        "rating": 4.3,
        "types": [
          "local_government_office",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Tower Bridge Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5013248,
            "lng": -0.07418990000000002
          },
          "viewport": {
            "northeast": {
              "lat": 51.5027331302915,
              "lng": -0.07275786970849799
            },
            "southwest": {
              "lat": 51.5000351697085,
              "lng": -0.07545583029150205
            }
          }
        },
        "name": "London Tower Bridge Apartments",
        "photos": [
          {
            "height": 2048,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105454001666052441552/photos\\">Will Tudor</a>"
            ],
            "width": 1536
          }
        ],
        "place_id": "ChIJ95XwOGgDdkgRF_9ttwN_ZeU",
        "rating": 4.8,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "The Circle, Queen Elizabeth Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.504575,
            "lng": -0.08267500000000001
          },
          "viewport": {
            "northeast": {
              "lat": 51.5058706302915,
              "lng": -0.08134066970849796
            },
            "southwest": {
              "lat": 51.5031726697085,
              "lng": -0.08403863029150203
            }
          }
        },
        "name": "Hilton London Tower Bridge",
        "photos": [
          {
            "height": 1292,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105612670614630819677/photos\\">Hilton London Tower Bridge</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJry4CB1ADdkgRrKALccYikP8",
        "rating": 4.3,
        "types": [
          "gym",
          "lodging",
          "health",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "5 More London Place, Tooley Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5053748,
            "lng": -0.07545369999999998
          },
          "viewport": {
            "northeast": {
              "lat": 51.5067237802915,
              "lng": -0.07410471970849795
            },
            "southwest": {
              "lat": 51.5040258197085,
              "lng": -0.07680268029150202
            }
          }
        },
        "name": "Tower Bridge Events",
        "photos": [
          {
            "height": 2760,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/100936675305663765256/photos\\">Ana I R</a>"
            ],
            "width": 4912
          }
        ],
        "place_id": "ChIJfwvq3UUDdkgRoz4C0pqI_hM",
        "rating": 5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Tower Bridge Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.506249,
            "lng": -0.08782179999999999
          },
          "viewport": {
            "northeast": {
              "lat": 51.50765733029149,
              "lng": -0.08646136970849796
            },
            "southwest": {
              "lat": 51.50495936970849,
              "lng": -0.08915933029150204
            }
          }
        },
        "name": "Tower Bridge",
        "photos": [
          {
            "height": 1536,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105520278337027169632/photos\\">Milena Andrade</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJw_TLz1ADdkgR2rZBO7EugYU",
        "types": [
          "museum",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "6 Tooley Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.49878,
            "lng": -0.07944300000000001
          },
          "viewport": {
            "northeast": {
              "lat": 51.5001130802915,
              "lng": -0.07800666970849797
            },
            "southwest": {
              "lat": 51.4974151197085,
              "lng": -0.08070463029150203
            }
          }
        },
        "name": "Aparthotels London Tower Bridge",
        "place_id": "ChIJmzcReUMDdkgRINsjCfBlMaE",
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "153 Tower Bridge Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5116032,
            "lng": -0.07193560000000002
          },
          "viewport": {
            "northeast": {
              "lat": 51.5130591802915,
              "lng": -0.07005955000000001
            },
            "southwest": {
              "lat": 51.5103612197085,
              "lng": -0.07309014999999999
            }
          }
        },
        "name": "Grange Tower Bridge Hotel",
        "photos": [
          {
            "height": 827,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110233233997487643888/photos\\">Grange Tower Bridge Hotel</a>"
            ],
            "width": 1011
          }
        ],
        "place_id": "ChIJjQzyQ0oDdkgRCTspAw2qeoM",
        "rating": 4.3,
        "types": [
          "bar",
          "lodging",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "45 Prescot Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.510644,
            "lng": -0.0773503
          },
          "viewport": {
            "northeast": {
              "lat": 51.51207593029149,
              "lng": -0.07603716970849796
            },
            "southwest": {
              "lat": 51.5093779697085,
              "lng": -0.07873513029150203
            }
          }
        },
        "name": "Hotel Novotel London Tower Bridge",
        "photos": [
          {
            "height": 1365,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/100488652756737800417/photos\\">Hotel Novotel London Tower Bridge</a>"
            ],
            "width": 1368
          }
        ],
        "place_id": "ChIJtx22FUwDdkgRpORimUrdYlQ",
        "rating": 4.4,
        "types": [
          "gym",
          "spa",
          "lodging",
          "health",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "10 Pepys Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.50937260000001,
            "lng": -0.0811137
          },
          "viewport": {
            "northeast": {
              "lat": 51.5106560802915,
              "lng": -0.07969531970849797
            },
            "southwest": {
              "lat": 51.5079581197085,
              "lng": -0.08239328029150203
            }
          }
        },
        "name": "hub by Premier Inn London Tower Bridge",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 500,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/112076531914394634560/photos\\">hub by Premier Inn London Tower Bridge</a>"
            ],
            "width": 800
          }
        ],
        "place_id": "ChIJsxIeDE4DdkgRkJabQiaacvo",
        "rating": 4.5,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "28 Great Tower Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.511413,
            "lng": -0.07505799999999999
          },
          "viewport": {
            "northeast": {
              "lat": 51.5127488802915,
              "lng": -0.07383451970849796
            },
            "southwest": {
              "lat": 51.5100509197085,
              "lng": -0.07653248029150202
            }
          }
        },
        "name": "Urbanest Tower Bridge",
        "photos": [
          {
            "height": 1152,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106806956979402949881/photos\\">Urbanest Tower Bridge</a>"
            ],
            "width": 1153
          }
        ],
        "place_id": "ChIJpwOQlksDdkgRGuyD_x-Ny_c",
        "rating": 4.3,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "52-56 Minories, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.51068799999999,
            "lng": -0.06904339999999998
          },
          "viewport": {
            "northeast": {
              "lat": 51.5120768302915,
              "lng": -0.06770526970849795
            },
            "southwest": {
              "lat": 51.5093788697085,
              "lng": -0.07040323029150201
            }
          }
        },
        "name": "Simmons Bar | Tower Bridge",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 684,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/109027079480853407255/photos\\">Simmons Bar | Tower Bridge</a>"
            ],
            "width": 682
          }
        ],
        "place_id": "ChIJU2RB6TUDdkgRdMFTLtY_DYU",
        "rating": 4.2,
        "types": [
          "bar",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "61 Royal Mint Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.51105769999999,
            "lng": -0.0735058
          },
          "viewport": {
            "northeast": {
              "lat": 51.51248148029149,
              "lng": -0.07250341970849797
            },
            "southwest": {
              "lat": 51.5097835197085,
              "lng": -0.07520138029150203
            }
          }
        },
        "name": "Travelodge London Central Tower Bridge Hotel",
        "photos": [
          {
            "height": 3264,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111083370728700444566/photos\\">Joshua Roberts</a>"
            ],
            "width": 2448
          }
        ],
        "place_id": "ChIJaZLTtUsDdkgR1vp_B9esXkY",
        "rating": 3.5,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Lloyds Court Business Centre, 1 Goodman's Yard, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.51078649999999,
            "lng": -0.06956659999999999
          },
          "viewport": {
            "northeast": {
              "lat": 51.5120918302915,
              "lng": -0.06819416970849797
            },
            "southwest": {
              "lat": 51.5093938697085,
              "lng": -0.07089213029150203
            }
          }
        },
        "name": "UPark Tower Bridge",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 290,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/109581396717912031371/photos\\">UPark Tower Bridge</a>"
            ],
            "width": 783
          }
        ],
        "place_id": "ChIJ9YDg7TUDdkgRFtqudu9WKb8",
        "rating": 4.6,
        "types": [
          "parking",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Arch 57, Royal Mint Street, Tower Hill, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5053084,
            "lng": -0.080375
          },
          "viewport": {
            "northeast": {
              "lat": 51.50651573029149,
              "lng": -0.07901661970849796
            },
            "southwest": {
              "lat": 51.5038177697085,
              "lng": -0.08171458029150203
            }
          }
        },
        "name": "Third Space Tower Bridge",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1499,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114828927684740788803/photos\\">Third Space Tower Bridge</a>"
            ],
            "width": 2000
          }
        ],
        "place_id": "ChIJVb3vOGgDdkgR_rZeet9tk4A",
        "rating": 4.2,
        "types": [
          "gym",
          "health",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "2b More London Riverside, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.511738,
            "lng": -0.074654
          },
          "viewport": {
            "northeast": {
              "lat": 51.5132448302915,
              "lng": -0.07329806970849796
            },
            "southwest": {
              "lat": 51.51054686970851,
              "lng": -0.07599603029150202
            }
          }
        },
        "name": "Virgin Active Tower Bridge",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 591,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110240539061206958780/photos\\">Virgin Active Tower Bridge</a>"
            ],
            "width": 591
          }
        ],
        "place_id": "ChIJM8vdm0sDdkgRrLIdpufqW1A",
        "rating": 4,
        "types": [
          "gym",
          "health",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Ibex House, 1 Haydon Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5088231,
            "lng": -0.06930000000000001
          },
          "viewport": {
            "northeast": {
              "lat": 51.5100340302915,
              "lng": -0.06794866970849796
            },
            "southwest": {
              "lat": 51.5073360697085,
              "lng": -0.07064663029150203
            }
          }
        },
        "name": "Tower Bridge Business Centre",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1000,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/102016519402232918367/photos\\">Tower Bridge Business Centre</a>"
            ],
            "width": 1002
          }
        ],
        "place_id": "ChIJ8WsGFDYDdkgRoNtyNDefaTg",
        "rating": 4.6,
        "types": [
          "real_estate_agency",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "46-48 East Smithfield, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5015551,
            "lng": -0.07723239999999999
          },
          "viewport": {
            "northeast": {
              "lat": 51.50300563029149,
              "lng": -0.07578456970849798
            },
            "southwest": {
              "lat": 51.50030766970849,
              "lng": -0.07848253029150204
            }
          }
        },
        "name": "Tower Bridge Primary School",
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/118041140286495893309/photos\\">Pawel Szwed</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJO4UjSUQDdkgRWAmkeBfw3cI",
        "rating": 3.9,
        "types": [
          "school",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Fair Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.4902017,
            "lng": -0.0807444
          },
          "viewport": {
            "northeast": {
              "lat": 51.4916181302915,
              "lng": -0.07929296970849797
            },
            "southwest": {
              "lat": 51.4889201697085,
              "lng": -0.08199093029150203
            }
          }
        },
        "name": "Eurotraveller Hotel-Premier (Nr. Tower Bridge) SE1",
        "photos": [
          {
            "height": 2322,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111879715127071236133/photos\\">Turbo Sierra</a>"
            ],
            "width": 4128
          }
        ],
        "place_id": "ChIJb_aWlGgDdkgRQ3SyJQ9ZKjg",
        "rating": 3.6,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "194-202 Old Kent Road, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.4943353,
            "lng": -0.06121120000000001
          },
          "viewport": {
            "northeast": {
              "lat": 51.49621648029149,
              "lng": -0.05999261970849797
            },
            "southwest": {
              "lat": 51.49351851970849,
              "lng": -0.06269058029150203
            }
          }
        },
        "name": "Workspace The Biscuit Factory",
        "photos": [
          {
            "height": 1152,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106174482471677201499/photos\\">Diego Tarabola</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJ6Zvnlz0DdkgR0S5GVQHBpV8",
        "rating": 3.9,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "100 Clements Road, Southwark, London"
      }
    ]
  },
  "Trafalgar Square": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 51.508039,
            "lng": -0.128069
          },
          "viewport": {
            "northeast": {
              "lat": 51.50943903029149,
              "lng": -0.1262822
            },
            "southwest": {
              "lat": 51.5067410697085,
              "lng": -0.1298049
            }
          }
        },
        "name": "Trafalgar Square",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2322,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111864637642288230504/photos\\">Debbie Daniels</a>"
            ],
            "width": 4128
          }
        ],
        "place_id": "ChIJH-tBOc4EdkgRJ8aJ8P1CUxo",
        "rating": 4.5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Trafalgar Square, Westminster, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.507374,
            "lng": -0.129135
          },
          "viewport": {
            "northeast": {
              "lat": 51.5087373802915,
              "lng": -0.127724369708498
            },
            "southwest": {
              "lat": 51.5060394197085,
              "lng": -0.130422330291502
            }
          }
        },
        "name": "The Trafalgar Hotel",
        "photos": [
          {
            "height": 1360,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110199432199849165437/photos\\">The Trafalgar Hotel</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJt2xYKc4EdkgRiSOCe1hI2zU",
        "rating": 4.1,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "2 Spring Gardens, Trafalgar Square, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.50818599999999,
            "lng": -0.1287113
          },
          "viewport": {
            "northeast": {
              "lat": 51.5095160302915,
              "lng": -0.127484469708498
            },
            "southwest": {
              "lat": 51.5068180697085,
              "lng": -0.130182430291502
            }
          }
        },
        "name": "The Fourth Plinth",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/117951404842553926995/photos\\">Milda Pumputyt\\u0117</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJU3TKI84EdkgRJbpX7knbX0Y",
        "rating": 3.4,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Trafalgar Square, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5079454,
            "lng": -0.1274292
          },
          "viewport": {
            "northeast": {
              "lat": 51.5092997302915,
              "lng": -0.125936769708498
            },
            "southwest": {
              "lat": 51.5066017697085,
              "lng": -0.128634730291502
            }
          }
        },
        "name": "Major General Sir Henry Havelock",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2304,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101290781179116358064/photos\\">Mihai Andrei</a>"
            ],
            "width": 3456
          }
        ],
        "place_id": "ChIJvaxWRs4EdkgRtGL_swhqUFg",
        "rating": 4.2,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Trafalgar Square, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.50913300000001,
            "lng": -0.130189
          },
          "viewport": {
            "northeast": {
              "lat": 51.51046633029149,
              "lng": -0.128964769708498
            },
            "southwest": {
              "lat": 51.50776836970849,
              "lng": -0.131662730291502
            }
          }
        },
        "name": "Thistle Trafalgar Square",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2952,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106685232428332616600/photos\\">Malcolm St Pierre</a>"
            ],
            "width": 5248
          }
        ],
        "place_id": "ChIJexYS8tEEdkgRoOLWbHv1IHg",
        "rating": 4.1,
        "types": [
          "bar",
          "lodging",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Whitcomb Street, Trafalgar Square, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.50698300000001,
            "lng": -0.12601
          },
          "viewport": {
            "northeast": {
              "lat": 51.5084208802915,
              "lng": -0.124618869708498
            },
            "southwest": {
              "lat": 51.5057229197085,
              "lng": -0.127316830291502
            }
          }
        },
        "name": "Club Quarters Hotel, Trafalgar Square",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1365,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/104791046732880218123/photos\\">Club Quarters Hotel, Trafalgar Square</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJkW6HU84EdkgRh5r5Qd4Z0Rc",
        "rating": 4.1,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "8 Northumberland Avenue, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.50786129999999,
            "lng": -0.1273563
          },
          "viewport": {
            "northeast": {
              "lat": 51.50925468029151,
              "lng": -0.125917119708498
            },
            "southwest": {
              "lat": 51.50655671970851,
              "lng": -0.128615080291502
            }
          }
        },
        "name": "Charing Cross Underground Station",
        "photos": [
          {
            "height": 1126,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101195241239635894319/photos\\">Pascal Rary</a>"
            ],
            "width": 1600
          }
        ],
        "place_id": "ChIJhy-eTs4EdkgRjvVoQxhTSYg",
        "rating": 4.4,
        "types": [
          "subway_station",
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Trafalgar Square, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.507752,
            "lng": -0.1279302
          },
          "viewport": {
            "northeast": {
              "lat": 51.5090184802915,
              "lng": -0.126531519708498
            },
            "southwest": {
              "lat": 51.5063205197085,
              "lng": -0.129229480291502
            }
          }
        },
        "name": "Nelson's Column",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2160,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/116577675681734261882/photos\\">Maff Mace</a>"
            ],
            "width": 3840
          }
        ],
        "place_id": "ChIJVVXFOc4EdkgRtXIzryB6GLg",
        "rating": 4.6,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "5 Trafalgar Square, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.50709869999999,
            "lng": -0.1246815
          },
          "viewport": {
            "northeast": {
              "lat": 51.50840608029149,
              "lng": -0.123192569708498
            },
            "southwest": {
              "lat": 51.5057081197085,
              "lng": -0.125890530291502
            }
          }
        },
        "name": "Citadines Trafalgar Square London",
        "photos": [
          {
            "height": 496,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/108109482159017840390/photos\\">Citadines Trafalgar Square London</a>"
            ],
            "width": 745
          }
        ],
        "place_id": "ChIJdfveOc4EdkgRafyvBawIUJE",
        "rating": 4,
        "types": [
          "real_estate_agency",
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "18-21 Northumberland Avenue, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.50696900000001,
            "lng": -0.125992
          },
          "viewport": {
            "northeast": {
              "lat": 51.5084117802915,
              "lng": -0.124598519708498
            },
            "southwest": {
              "lat": 51.5057138197085,
              "lng": -0.127296480291502
            }
          }
        },
        "name": "The Grand At Trafalgar Square",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1270,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113326565793778142635/photos\\">The Grand At Trafalgar Square</a>"
            ],
            "width": 936
          }
        ],
        "place_id": "ChIJ78fbAc8EdkgRWG1dhffz9AY",
        "rating": 4.1,
        "types": [
          "bar",
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "8 Northumberland Avenue, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.507381,
            "lng": -0.1300773
          },
          "viewport": {
            "northeast": {
              "lat": 51.5088428802915,
              "lng": -0.128666019708498
            },
            "southwest": {
              "lat": 51.5061449197085,
              "lng": -0.131363980291502
            }
          }
        },
        "name": "Thai Square -Trafalgar Square",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1365,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/104111246635874032234/photos\\">ZAGAT</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJ7TpKztEEdkgRkuPDNp_g11g",
        "price_level": 2,
        "rating": 3.8,
        "types": [
          "bar",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "21-24 Cockspur Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5094343,
            "lng": -0.1260391
          },
          "viewport": {
            "northeast": {
              "lat": 51.5108144302915,
              "lng": -0.124617019708498
            },
            "southwest": {
              "lat": 51.5081164697085,
              "lng": -0.127314980291502
            }
          }
        },
        "name": "Trafalgar Square Post Office",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3120,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114718596078187958027/photos\\">Jyotheeswara Naidu Malapati</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJ8RRRec4EdkgR3oVYudtTsFs",
        "price_level": 2,
        "rating": 3.8,
        "types": [
          "post_office",
          "finance",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "24/28 William IV Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5062186,
            "lng": -0.1269459
          },
          "viewport": {
            "northeast": {
              "lat": 51.50756758029149,
              "lng": -0.125596919708498
            },
            "southwest": {
              "lat": 51.5048696197085,
              "lng": -0.128294880291502
            }
          }
        },
        "name": "Whitehall Trafalgar Square (Stop N)",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106147316764498333608/photos\\">Thomas Kuo</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJiZHSus8EdkgRCQg25xkFeV8",
        "rating": 5,
        "types": [
          "bus_station",
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "United Kingdom"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5069738,
            "lng": -0.1261354
          },
          "viewport": {
            "northeast": {
              "lat": 51.5084264802915,
              "lng": -0.124737219708498
            },
            "southwest": {
              "lat": 51.5057285197085,
              "lng": -0.127435180291502
            }
          }
        },
        "name": "Connections at Trafalgar Square",
        "place_id": "ChIJj25MAs8EdkgRpatOJnQ-rRE",
        "rating": 5,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "8 Northumberland Avenue, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5091474,
            "lng": -0.1305215
          },
          "viewport": {
            "northeast": {
              "lat": 51.51049528029149,
              "lng": -0.129176019708498
            },
            "southwest": {
              "lat": 51.50779731970849,
              "lng": -0.131873980291502
            }
          }
        },
        "name": "Pall Mall Barbers Trafalgar Square",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 518,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115208321764325274362/photos\\">Pall Mall Barbers Trafalgar Square</a>"
            ],
            "width": 518
          }
        ],
        "place_id": "ChIJ98778tEEdkgRho5r25wxdBc",
        "rating": 4.6,
        "types": [
          "hair_care",
          "health",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "27 Whitcomb Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.507645,
            "lng": -0.129519
          },
          "viewport": {
            "northeast": {
              "lat": 51.50899398029149,
              "lng": -0.128170019708498
            },
            "southwest": {
              "lat": 51.5062960197085,
              "lng": -0.130867980291502
            }
          }
        },
        "name": "Trafalgar Square (Stop B)",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106147316764498333608/photos\\">Thomas Kuo</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJi-1L19EEdkgR8oosmrvN1HM",
        "rating": 4,
        "types": [
          "bus_station",
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "United Kingdom"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.508681,
            "lng": -0.1262686
          },
          "viewport": {
            "northeast": {
              "lat": 51.5100299802915,
              "lng": -0.124919619708498
            },
            "southwest": {
              "lat": 51.5073320197085,
              "lng": -0.127617580291502
            }
          }
        },
        "name": "Trafalgar Square (Stop G)",
        "photos": [
          {
            "height": 4032,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106147316764498333608/photos\\">Thomas Kuo</a>"
            ],
            "width": 3024
          }
        ],
        "place_id": "ChIJF8MOis4EdkgRvYTmyBBp7pg",
        "rating": 5,
        "types": [
          "bus_station",
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "United Kingdom"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.506481,
            "lng": -0.127117
          },
          "viewport": {
            "northeast": {
              "lat": 51.5078299802915,
              "lng": -0.125768019708498
            },
            "southwest": {
              "lat": 51.5051320197085,
              "lng": -0.128465980291502
            }
          }
        },
        "name": "Whitehall Trafalgar Square (Stop M)",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106147316764498333608/photos\\">Thomas Kuo</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJ3_ZdsM8EdkgRxXNqFWmg_9g",
        "types": [
          "bus_station",
          "transit_station",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "United Kingdom"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5093918,
            "lng": -0.1267076
          },
          "viewport": {
            "northeast": {
              "lat": 51.5107836802915,
              "lng": -0.125331719708498
            },
            "southwest": {
              "lat": 51.5080857197085,
              "lng": -0.128029680291502
            }
          }
        },
        "name": "Flight Centre Trafalgar Square",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3120,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114718596078187958027/photos\\">Jyotheeswara Naidu Malapati</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJZf2Jlc4EdkgRe12hVC5LdC8",
        "types": [
          "travel_agency",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "7-8 St. Martin's Pl, William IV Street, London"
      },
      {
        "geometry": {
          "location": {
            "lat": 51.5071282,
            "lng": -0.1264511
          },
          "viewport": {
            "northeast": {
              "lat": 51.50853658029149,
              "lng": -0.125073969708498
            },
            "southwest": {
              "lat": 51.50583861970849,
              "lng": -0.127771930291502
            }
          }
        },
        "name": "BIANCO43 TRAFALGAR",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2448,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/104772325925830673549/photos\\">Alex H</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJ3TxJ_s4EdkgRhtf1nWI8Hdc",
        "rating": 4,
        "types": [
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "7 Northumberland Avenue, London"
      }
    ]
  }
}
""",

    }
)
