import sys

from django.test import TestCase

from tl_trailer.utilities.example_data import ExampleData
from unittest import skip


@skip("Deferred")
class GroupViewTest(TestCase):

    def __init__(self, context):
        TestCase.__init__(self, context)

    @staticmethod
    def setUpClass():
        TestCase.setUpClass()
        print(file=sys.stderr)
        sys.stderr.write('GroupViewTest: ')

    def setUp(self):
        self.example_data = ExampleData()
        self.test_group_id = self.example_data.euro_group.id
        self.url_get_params = '?type=Group&id=%d' % (self.test_group_id,)

    def test_group_layout(self):
        urlpath = '/trailer' + self.url_get_params
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '/map')
        self.assertContains(response, '/summary')
        self.assertContains(response, '/table')

    def test_group_summary(self):
        urlpath = '/trailer/summary' + self.url_get_params
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Name')
        self.assertContains(response, 'Europe')
        self.assertContains(response, 'Notes')
        self.assertContains(response, 'This example group has')

    def test_group_table(self):
        urlpath = '/trailer/table' + self.url_get_params
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Trails')
        self.assertContains(response, 'Barcelona')
        self.assertContains(response, 'Subgroups')
        self.assertContains(response, 'UK')

    def test_group_map(self):
        urlpath = '/trailer/map' + self.url_get_params
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Attempting to load map with bounds')

    def tearDown(self):
        pass

    @staticmethod
    def tearDownClass():
        TestCase.tearDownClass()
        print(file=sys.stderr)
