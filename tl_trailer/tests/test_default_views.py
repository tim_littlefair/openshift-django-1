import sys

from django.test import TestCase
from unittest import skip


@skip("Deferred")
class DefaultViewTest(TestCase):

    def __init__(self, context):
        TestCase.__init__(self, context)

    @staticmethod
    def setUpClass():
        TestCase.setUpClass()
        print(file=sys.stderr)
        sys.stderr.write('DefaultViewTest: ')

    def test_default_layout(self):
        urlpath = '/trailer'
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)

    def test_default_table(self):
        urlpath = '/trailer/table?'
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)

    def test_default_map(self):
        urlpath = '/trailer/map'
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)
        # print(str(response.content,'utf-8'),file=sys.stderr)
        self.assertContains(response, 'Attempting to load map with bounds')

    @staticmethod
    def tearDownClass():
        TestCase.tearDownClass()
        print(file=sys.stderr)
