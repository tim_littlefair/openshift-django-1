import sys

from django.test import TestCase
from unittest import skip


@skip("Deferred")
class ExampleViewTest(TestCase):

    def __init__(self, context):
        TestCase.__init__(self, context)

    @staticmethod
    def setUpClass():
        TestCase.setUpClass()
        print(file=sys.stderr)
        sys.stderr.write('ExampleViewTest: ')

    def setUp(self):
        pass

    def test_example_layout(self):
        urlpath = '/trailer/examples'
        response1 = self.client.get(urlpath)
        self.assertEqual(response1.status_code, 302)
        response2 = self.client.get(response1.url)
        self.assertEqual(response2.status_code, 200)
        # print(str(response2.content,'utf-8'), file=sys.stderr)
        self.assertContains(response2, '/map?type=Group&id=')

    def tearDown(self):
        pass

    @staticmethod
    def tearDownClass():
        TestCase.tearDownClass()
        print(file=sys.stderr)
