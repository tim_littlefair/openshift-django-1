import sys

from tl_trailer.models import CapacityError

from django.test import TestCase
from unittest import skip, skipIf

from tl_trailer.utilities.search_helper import SearchHelper

from . import test_level
from .edinburgh_entertainment import edinburgh_entertainment

search_helper = SearchHelper()


@skip("Deferred")
class EdinburghSearchTest(TestCase):

    def __init__(self, context):
        TestCase.__init__(self, context)

    @staticmethod
    def setUpClass():
        TestCase.setUpClass()
        search_helper.set_data(edinburgh_entertainment)
        print(file=sys.stderr)
        sys.stderr.write('EdinburghSearchTest: ')

    @skipIf(test_level > 0, "Suppressing tests which consume Google API quota")
    def test_locality_search(self):
        search_helper.locality_search()

    @skipIf(test_level > 0, "Suppressing tests which consume Google API quota")
    def test_single_keyword_search(self):
        search_helper.single_keyword_search()

    def test_builder(self):
        trail = search_helper.build_trail(15)
        self.assertIs(15, len(trail.zone_set.all()))

    def test_capacity(self):
        # In order to enable the test_capacity method to verify
        # behaviour when the database approaches capacity,
        # we limit the length of id's to 2 digits, which will
        # mean that the capacity will be 10 objects of any given type
        from tl_trailer.models import set_id_digits_for_test
        set_id_digits_for_test(2)
        with self.assertRaises(CapacityError):
            search_helper.build_trail(12)
        set_id_digits_for_test(None)

    @staticmethod
    def tearDownClass():
        TestCase.tearDownClass()
        SearchHelper.set_data(None, 'edinburgh_entertainment')
        print(file=sys.stderr)
