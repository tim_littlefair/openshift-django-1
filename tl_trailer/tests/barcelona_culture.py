# Generated Sat Jul 22 08:10:18 2017
# pylint:disable=line-too-long,too-many-lines,missing-docstring

from tl_trailer.utilities.tuple_types import ExampleSearchData

barcelona_culture = ExampleSearchData(
    'Barcelona, Spain',
    'Gaudi\\nArt\\nMonuments\\n',
    {
        'locality_result':

        # https://maps.googleapis.com/maps/api/place/textsearch/json?key=_redacted_&query=Barcelona%2C+Spain
        """
{
  "html_attributions": [],
  "results": [
    {
      "formatted_address": "Barcelona, Spain",
      "geometry": {
        "location": {
          "lat": 41.38506389999999,
          "lng": 2.1734035
        },
        "viewport": {
          "northeast": {
            "lat": 41.4695761,
            "lng": 2.2280099
          },
          "southwest": {
            "lat": 41.320004,
            "lng": 2.0695258
          }
        }
      },
      "name": "Barcelona",
      "photos": [
        {
          "height": 2160,
          "html_attributions": [
            "<a href=\\"https://maps.google.com/maps/contrib/100619618872182980262/photos\\">Jordi Tendero</a>"
          ],
          "width": 3840
        }
      ],
      "place_id": "ChIJ5TCOcRaYpBIRCmZHTz37sEQ",
      "types": [
        "locality",
        "political"
      ]
    }
  ],
  "status": "OK"
}
""",

        'keyword_results':

        # https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=_redacted_&location=41.3850639%2C2.1734035&radius=25000&keyword=Monuments
        """
{
  "Art": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 41.38177499999999,
            "lng": 2.176462
          },
          "viewport": {
            "northeast": {
              "lat": 41.3831208302915,
              "lng": 2.177806980291502
            },
            "southwest": {
              "lat": 41.3804228697085,
              "lng": 2.175109019708498
            }
          }
        },
        "name": "Artevistas Gallery",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1237,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/118024255603536181508/photos\\">Artevistas - Art Gallery</a>"
            ],
            "width": 1243
          }
        ],
        "place_id": "ChIJNZNSZviipBIR9jp0Fp4kxqA",
        "rating": 4.4,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Passatge del Cr\\u00e8dit, 4, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.39521799999999,
            "lng": 2.129372
          },
          "viewport": {
            "northeast": {
              "lat": 41.39658528029149,
              "lng": 2.130685280291502
            },
            "southwest": {
              "lat": 41.3938873197085,
              "lng": 2.127987319708498
            }
          }
        },
        "name": "Barcelona Academy of Art | Sarri\\u00e0",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 804,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/104869903296023232503/photos\\">Barcelona Academy of Art | Sarri\\u00e0</a>"
            ],
            "width": 804
          }
        ],
        "place_id": "ChIJj4TROGmYpBIRnBXQt7Szjas",
        "rating": 5,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Maria Auxiliadora, 9, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.38769509999999,
            "lng": 2.1623713
          },
          "viewport": {
            "northeast": {
              "lat": 41.3890699302915,
              "lng": 2.163755080291502
            },
            "southwest": {
              "lat": 41.3863719697085,
              "lng": 2.161057119708498
            }
          }
        },
        "name": "PRINCIPAL ART",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1536,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113800961379147485442/photos\\">PRINCIPAL ART</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJ0WErtHyipBIRyk1grgVadH4",
        "rating": 5,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer d'Enric Granados, 9, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.389302,
            "lng": 2.163223
          },
          "viewport": {
            "northeast": {
              "lat": 41.3906509802915,
              "lng": 2.164571980291502
            },
            "southwest": {
              "lat": 41.3879530197085,
              "lng": 2.161874019708498
            }
          }
        },
        "name": "Art Barcelona Asociaci\\u00f3n de Galer\\u00edas",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 315,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/112203077268847799460/photos\\">Art Barcelona Asociaci\\u00f3n de Galer\\u00edas</a>"
            ],
            "width": 850
          }
        ],
        "place_id": "ChIJP-DqFo2ipBIRwS3Vy05nr8I",
        "rating": 4,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "C/ Consell de Cent, 315 - Entl 1\\u00aa, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3814665,
            "lng": 2.1799818
          },
          "viewport": {
            "northeast": {
              "lat": 41.3828154802915,
              "lng": 2.181330780291502
            },
            "southwest": {
              "lat": 41.3801175197085,
              "lng": 2.178632819708498
            }
          }
        },
        "name": "La Carboneria. Drap Art",
        "photos": [
          {
            "height": 4032,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114559431397050372316/photos\\">C\\u00e9sar Parga</a>"
            ],
            "width": 3024
          }
        ],
        "place_id": "ChIJAQAAQPiipBIRclND4SNuOK4",
        "rating": 4.4,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Calle Groc, 1, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3810653,
            "lng": 2.1781274
          },
          "viewport": {
            "northeast": {
              "lat": 41.38242983029149,
              "lng": 2.179491580291502
            },
            "southwest": {
              "lat": 41.37973186970849,
              "lng": 2.176793619708498
            }
          }
        },
        "name": "Base Elements",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 273,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101885522639573254453/photos\\">Base Elements</a>"
            ],
            "width": 273
          }
        ],
        "place_id": "ChIJn645y_iipBIRaQv0NuxgaJM",
        "rating": 4.7,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer del Palau, 6, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3888444,
            "lng": 2.1805463
          },
          "viewport": {
            "northeast": {
              "lat": 41.3901807802915,
              "lng": 2.181988880291502
            },
            "southwest": {
              "lat": 41.3874828197085,
              "lng": 2.179290919708498
            }
          }
        },
        "name": "Galer\\u00eda Max\\u00f3",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3456,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105558052831842224313/photos\\">von crystjan</a>"
            ],
            "width": 5184
          }
        ],
        "place_id": "ChIJm6357PyipBIRd00SDpjQPbU",
        "rating": 4.9,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer del Portal Nou, 29, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.38984689999999,
            "lng": 2.1746978
          },
          "viewport": {
            "northeast": {
              "lat": 41.3911341802915,
              "lng": 2.176087830291502
            },
            "southwest": {
              "lat": 41.3884362197085,
              "lng": 2.173389869708498
            }
          }
        },
        "name": "Bed & Art Barcelona",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2592,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/103070099964951925276/photos\\">Bed &amp; Art Barcelona</a>"
            ],
            "width": 3888
          }
        ],
        "place_id": "ChIJ0xfNoPqipBIRCJ5lpbutGA4",
        "rating": 3,
        "types": [
          "art_gallery",
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Ronda de Sant Pere, 25, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3924894,
            "lng": 2.176512999999999
          },
          "viewport": {
            "northeast": {
              "lat": 41.39383838029149,
              "lng": 2.177861980291501
            },
            "southwest": {
              "lat": 41.39114041970849,
              "lng": 2.175164019708498
            }
          }
        },
        "name": "Artur Ramon Art",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2033,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106178232400003689705/photos\\">Artur Ramon Art</a>"
            ],
            "width": 2646
          }
        ],
        "place_id": "ChIJb2Plf_eipBIR8Nym8HDcMgg",
        "rating": 5,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Bail\\u00e8n, 19, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.384345,
            "lng": 2.151522
          },
          "viewport": {
            "northeast": {
              "lat": 41.3856629802915,
              "lng": 2.152893530291502
            },
            "southwest": {
              "lat": 41.3829650197085,
              "lng": 2.150195569708498
            }
          }
        },
        "name": "Sercotel Amister Art Hotel Barcelona",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1360,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113620712172737373594/photos\\">Sercotel Amister Art Hotel Barcelona</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJf0AI5oaipBIR0wGza6ackao",
        "rating": 4.4,
        "types": [
          "art_gallery",
          "lodging",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Avinguda de Roma, 93-95, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3845809,
            "lng": 2.1762517
          },
          "viewport": {
            "northeast": {
              "lat": 41.3860168302915,
              "lng": 2.177737080291501
            },
            "southwest": {
              "lat": 41.3833188697085,
              "lng": 2.175039119708498
            }
          }
        },
        "name": "Villa del Arte Galleries Barcelona",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1840,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107009538535421932650/photos\\">Neus Calderon</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJC0AcBfmipBIRqmaCXjbxBVc",
        "rating": 4.2,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de la Tapineria, 39, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.54868889999999,
            "lng": 2.1084789
          },
          "viewport": {
            "northeast": {
              "lat": 41.55001438029149,
              "lng": 2.109735680291501
            },
            "southwest": {
              "lat": 41.5473164197085,
              "lng": 2.107037719708498
            }
          }
        },
        "name": "Acad\\u00e8mia de Belles Arts de Sabadell Fundaci\\u00f3 Privada",
        "photos": [
          {
            "height": 2906,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114118451392288609524/photos\\">Pako Valera</a>"
            ],
            "width": 3946
          }
        ],
        "place_id": "ChIJyVECswOVpBIRvN_SF00Sp6g",
        "rating": 4.6,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Passeig de Manresa, 22, Sabadell"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.389013,
            "lng": 2.176146
          },
          "viewport": {
            "northeast": {
              "lat": 41.3903910302915,
              "lng": 2.177452980291501
            },
            "southwest": {
              "lat": 41.3876930697085,
              "lng": 2.174755019708498
            }
          }
        },
        "name": "Galeria Senda - Art Gallery",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 286,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/108230262469108959841/photos\\">Galeria Senda - Art Gallery</a>"
            ],
            "width": 284
          }
        ],
        "place_id": "ChIJDZD5To2ipBIRBdRsXUYKhQo",
        "rating": 4.7,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Trafalgar, 32, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.41723400000001,
            "lng": 2.18974
          },
          "viewport": {
            "northeast": {
              "lat": 41.4184240302915,
              "lng": 2.190869530291502
            },
            "southwest": {
              "lat": 41.41572606970851,
              "lng": 2.188171569708498
            }
          }
        },
        "name": "Espronceda Center for Art & Culture",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 545,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/102942355664905142288/photos\\">Espronceda Center for Art &amp; Culture</a>"
            ],
            "width": 818
          }
        ],
        "place_id": "ChIJyZJPcCyjpBIR-31DtwfhTnE",
        "rating": 4.3,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Nave 4 y 5, Carrer d'Espronceda, 326, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3845912,
            "lng": 2.1801288
          },
          "viewport": {
            "northeast": {
              "lat": 41.3859530802915,
              "lng": 2.181494980291502
            },
            "southwest": {
              "lat": 41.3832551197085,
              "lng": 2.178797019708498
            }
          }
        },
        "name": "Fousion Gallery",
        "photos": [
          {
            "height": 678,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101130766071380735037/photos\\">Fousion Gallery</a>"
            ],
            "width": 1024
          }
        ],
        "place_id": "ChIJ0yrhuP6ipBIRZQ2qy3PToxM",
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de la Carassa, 4, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.38252749999999,
            "lng": 2.1815062
          },
          "viewport": {
            "northeast": {
              "lat": 41.38390478029149,
              "lng": 2.182886780291502
            },
            "southwest": {
              "lat": 41.3812068197085,
              "lng": 2.180188819708498
            }
          }
        },
        "name": "Collage Art & Cocktail Social Club",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 597,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/108273081365840429961/photos\\">Collage Art &amp; Cocktail Social Club</a>"
            ],
            "width": 800
          }
        ],
        "place_id": "ChIJ2_JtBv-ipBIR5dPxj7TWNuc",
        "rating": 4.7,
        "types": [
          "art_gallery",
          "cafe",
          "bar",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer dels Consellers, 4, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3946233,
            "lng": 2.1712609
          },
          "viewport": {
            "northeast": {
              "lat": 41.3959722802915,
              "lng": 2.172609880291502
            },
            "southwest": {
              "lat": 41.3932743197085,
              "lng": 2.169911919708498
            }
          }
        },
        "name": "Art Fusion Bcn",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 640,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110513173903348472426/photos\\">Art Fusion Bcn</a>"
            ],
            "width": 960
          }
        ],
        "place_id": "ChIJLXpZwe6ipBIRTfN-vLzhWr4",
        "rating": 3.8,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Calle Gerona, 67 bxs, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3785918,
            "lng": 2.164623499999999
          },
          "viewport": {
            "northeast": {
              "lat": 41.3799390802915,
              "lng": 2.165968830291502
            },
            "southwest": {
              "lat": 41.3772411197085,
              "lng": 2.163270869708498
            }
          }
        },
        "name": "aDa Art Gallery",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 480,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/106863924965966892948/photos\\">aDa Art Gallery</a>"
            ],
            "width": 720
          }
        ],
        "place_id": "ChIJb-Bpf2CipBIRMrudCkR7iCY",
        "rating": 5,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer dels Salvador, 8, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.54750209999999,
            "lng": 2.1061942
          },
          "viewport": {
            "northeast": {
              "lat": 41.5488470302915,
              "lng": 2.107549630291502
            },
            "southwest": {
              "lat": 41.5461490697085,
              "lng": 2.104851669708498
            }
          }
        },
        "name": "Genial Restaurant & Art Gallery",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2268,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111473905886976881266/photos\\">Xavier Llamas</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJG_9EBAKVpBIRfkyJNg6Hy7A",
        "rating": 4.4,
        "types": [
          "art_gallery",
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de l'Escola Industrial, 24, Sabadell"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3885271,
            "lng": 2.1776826
          },
          "viewport": {
            "northeast": {
              "lat": 41.3898978802915,
              "lng": 2.179058080291502
            },
            "southwest": {
              "lat": 41.3871999197085,
              "lng": 2.176360119708499
            }
          }
        },
        "name": "Anaglifos Art Factory",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 683,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114712530895338433856/photos\\">Anaglifos Art Factory</a>"
            ],
            "width": 1024
          }
        ],
        "place_id": "ChIJ_bVfD_uipBIRpPZg8GA7Unc",
        "rating": 4.7,
        "types": [
          "art_gallery",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer d'En M\\u00f2nec, 17, Barcelona"
      }
    ]
  },
  "Gaudi": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 41.4036299,
            "lng": 2.1743558
          },
          "viewport": {
            "northeast": {
              "lat": 41.40489838029151,
              "lng": 2.175857949999999
            },
            "southwest": {
              "lat": 41.40220041970851,
              "lng": 2.17294095
            }
          }
        },
        "name": "La Sagrada Familia",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2000,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/100281120616068081615/photos\\">Aleksandr Nagibin</a>"
            ],
            "width": 3008
          }
        ],
        "place_id": "ChIJk_s92NyipBIRUMnDG8Kq2Js",
        "rating": 4.7,
        "types": [
          "church",
          "place_of_worship",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Mallorca, 401, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.384533,
            "lng": 2.176133
          },
          "viewport": {
            "northeast": {
              "lat": 41.38588533029149,
              "lng": 2.177485330291502
            },
            "southwest": {
              "lat": 41.3831873697085,
              "lng": 2.174787369708498
            }
          }
        },
        "name": "The Gaud\\u00ed Exhibition Center",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1355,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113726185743887524397/photos\\">Gaud\\u00ed Exhibition Center - MDB</a>"
            ],
            "width": 1636
          }
        ],
        "place_id": "ChIJZZnal_mipBIRKFeO-w8CsBU",
        "rating": 4.4,
        "types": [
          "museum",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Placita de la Seu, 7, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.37893500000001,
            "lng": 2.173997
          },
          "viewport": {
            "northeast": {
              "lat": 41.3802622802915,
              "lng": 2.175384530291502
            },
            "southwest": {
              "lat": 41.3775643197085,
              "lng": 2.172686569708498
            }
          }
        },
        "name": "Hotel Gaud\\u00ed",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 538,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105754802876879433679/photos\\">Hotel Gaud\\u00ed</a>"
            ],
            "width": 538
          }
        ],
        "place_id": "ChIJl763JViipBIRHHno1JPnoss",
        "rating": 4.1,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer Nou de la Rambla, 12, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4034978,
            "lng": 2.1506455
          },
          "viewport": {
            "northeast": {
              "lat": 41.4047637802915,
              "lng": 2.151951730291502
            },
            "southwest": {
              "lat": 41.4020658197085,
              "lng": 2.149253769708498
            }
          }
        },
        "name": "Casa Vicens Gaud\\u00ed",
        "photos": [
          {
            "height": 1871,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111734587992213643457/photos\\">francisco fernandez carrillo</a>"
            ],
            "width": 2262
          }
        ],
        "place_id": "ChIJoYA0MqKipBIRRwJG2R8H8s4",
        "rating": 3.8,
        "types": [
          "museum",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de les Carolines, 20, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4124873,
            "lng": 2.1536647
          },
          "viewport": {
            "northeast": {
              "lat": 41.41385943029149,
              "lng": 2.155046280291502
            },
            "southwest": {
              "lat": 41.4111614697085,
              "lng": 2.152348319708498
            }
          }
        },
        "name": "Gaud\\u00ed Experi\\u00e8ncia",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3072,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/116854891905978971156/photos\\">\\u0413\\u0430\\u043b\\u0438\\u043d\\u0430 \\u041f</a>"
            ],
            "width": 4608
          }
        ],
        "place_id": "ChIJAUw4wK-ipBIRYL5PIklnhfA",
        "rating": 4,
        "types": [
          "museum",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Larrard, 41, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4144423,
            "lng": 2.1535638
          },
          "viewport": {
            "northeast": {
              "lat": 41.4157833302915,
              "lng": 2.154916280291502
            },
            "southwest": {
              "lat": 41.4130853697085,
              "lng": 2.152218319708498
            }
          }
        },
        "name": "Gaud\\u00ed House Museum",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1836,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/108251191449680678315/photos\\">Liv Karine Smestad</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJ4eD9ztyipBIR_g1BtyluWPM",
        "rating": 3.8,
        "types": [
          "museum",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Park G\\u00fcell, Carretera del Carmel, 23A, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3916384,
            "lng": 2.164769799999999
          },
          "viewport": {
            "northeast": {
              "lat": 41.3929537302915,
              "lng": 2.166091280291501
            },
            "southwest": {
              "lat": 41.3902557697085,
              "lng": 2.163393319708498
            }
          }
        },
        "name": "Casa Batll\\u00f3",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 502,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105588566109647875620/photos\\">Casa Batll\\u00f3</a>"
            ],
            "width": 672
          }
        ],
        "place_id": "ChIJYUFLSe2ipBIRD04uni940kA",
        "rating": 4.6,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Passeig de Gr\\u00e0cia, 43, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.36387699999999,
            "lng": 2.027846
          },
          "viewport": {
            "northeast": {
              "lat": 41.3650007302915,
              "lng": 2.028902680291502
            },
            "southwest": {
              "lat": 41.3623027697085,
              "lng": 2.026204719708498
            }
          }
        },
        "name": "Gaud\\u00ed's Crypt",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2336,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/104236419670859183285/photos\\">Elisenda Esteve</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJSQjfYgmbpBIRhQB-ZudasG8",
        "rating": 4.2,
        "types": [
          "church",
          "place_of_worship",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Calle Claudi G\\u00fcell, Col\\u00f2nia G\\u00fcell, Santa Coloma de Cervell\\u00f3"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.409521,
            "lng": 2.1267202
          },
          "viewport": {
            "northeast": {
              "lat": 41.4108616802915,
              "lng": 2.127804930291502
            },
            "southwest": {
              "lat": 41.4081637197085,
              "lng": 2.125106969708498
            }
          }
        },
        "name": "Torre Bellesguard",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3456,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107612813406572078188/photos\\">Jaume Arisa</a>"
            ],
            "width": 4608
          }
        ],
        "place_id": "ChIJRaITNRaYpBIRUpIaGZLkkZQ",
        "rating": 4.5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Bellesguard, 16, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4010845,
            "lng": 2.164211599999999
          },
          "viewport": {
            "northeast": {
              "lat": 41.4024316302915,
              "lng": 2.165563430291502
            },
            "southwest": {
              "lat": 41.3997336697085,
              "lng": 2.162865469708498
            }
          }
        },
        "name": "Apartments Gaudi Barcelona",
        "photos": [
          {
            "height": 2458,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115227144572956381634/photos\\">Hannes Hausegger</a>"
            ],
            "width": 3503
          }
        ],
        "place_id": "ChIJIagA_b-ipBIRn5TOJOL_xbQ",
        "rating": 4.2,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Camprodon, 27, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4102365,
            "lng": 2.1745059
          },
          "viewport": {
            "northeast": {
              "lat": 41.4115847802915,
              "lng": 2.175808930291502
            },
            "southwest": {
              "lat": 41.4088868197085,
              "lng": 2.173110969708499
            }
          }
        },
        "name": "Avinguda Gaudi Barcelonastuff Apartments",
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101441930908416142242/photos\\">A1 B1</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJ92IzrdqipBIR017NTTTqN60",
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Avinguda de Gaud\\u00ed, 66, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4046225,
            "lng": 2.1757737
          },
          "viewport": {
            "northeast": {
              "lat": 41.4059714802915,
              "lng": 2.177122680291501
            },
            "southwest": {
              "lat": 41.4032735197085,
              "lng": 2.174424719708498
            }
          }
        },
        "name": "Plaza de Gaud\\u00ed",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1152,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107161120273641446230/photos\\">George Gusein</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJH7GJwNyipBIRtwSx3flHQPw",
        "rating": 4.4,
        "types": [
          "park",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Lepant, 278, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4046678,
            "lng": 2.1760547
          },
          "viewport": {
            "northeast": {
              "lat": 41.4060809802915,
              "lng": 2.177487080291502
            },
            "southwest": {
              "lat": 41.4033830197085,
              "lng": 2.174789119708498
            }
          }
        },
        "name": "Gaudi's Nest Apartment",
        "photos": [
          {
            "height": 3648,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/109194814162785108339/photos\\">Da Redd</a>"
            ],
            "width": 5472
          }
        ],
        "place_id": "ChIJs9-Fk9yipBIRgAwka4Z1CAM",
        "rating": 4.6,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Lepant, 273, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.40658620000001,
            "lng": 2.1741537
          },
          "viewport": {
            "northeast": {
              "lat": 41.4079370802915,
              "lng": 2.175594530291502
            },
            "southwest": {
              "lat": 41.4052391197085,
              "lng": 2.172896569708498
            }
          }
        },
        "name": "Filat\\u00e8lia Numism\\u00e0tica Gaud\\u00ed",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3104,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107249493218894544150/photos\\">marisa puig</a>"
            ],
            "width": 1746
          }
        ],
        "place_id": "ChIJMwjMs8SipBIRweKaxwMrA7I",
        "rating": 5,
        "types": [
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Avinguda de Gaud\\u00ed, 27, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4022055,
            "lng": 2.1752095
          },
          "viewport": {
            "northeast": {
              "lat": 41.40349518029149,
              "lng": 2.176478580291501
            },
            "southwest": {
              "lat": 41.40079721970849,
              "lng": 2.173780619708498
            }
          }
        },
        "name": "Gaud\\u00ed Bakery",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1200,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/103738677428782928960/photos\\">Gaud\\u00ed Bakery</a>"
            ],
            "width": 1656
          }
        ],
        "place_id": "ChIJJfU1ON2ipBIRkqvyt3Ziw6I",
        "rating": 4,
        "types": [
          "bakery",
          "cafe",
          "food",
          "store",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Sardenya, 298, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.39538049999999,
            "lng": 2.1619614
          },
          "viewport": {
            "northeast": {
              "lat": 41.39655243029149,
              "lng": 2.163263030291502
            },
            "southwest": {
              "lat": 41.39385446970849,
              "lng": 2.160565069708498
            }
          }
        },
        "name": "Casa Mil\\u00e0",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 4000,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111241265556733563697/photos\\">Ruediger Lunde</a>"
            ],
            "width": 6000
          }
        ],
        "place_id": "ChIJ1eGKmZOipBIRah43T2Kjn8Q",
        "rating": 4.5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Proven\\u00e7a, 261-265, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4097881,
            "lng": 2.1728581
          },
          "viewport": {
            "northeast": {
              "lat": 41.41120773029149,
              "lng": 2.174111430291501
            },
            "southwest": {
              "lat": 41.40850976970849,
              "lng": 2.171413469708498
            }
          }
        },
        "name": "Gaudi House",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 4608,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/105606527536154225398/photos\\">Nigel Cummings</a>"
            ],
            "width": 3456
          }
        ],
        "place_id": "ChIJ-UZybsWipBIRlEEgfZqFAYI",
        "rating": 3.5,
        "types": [
          "real_estate_agency",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Sant Antoni Maria Claret, 192, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.40742419999999,
            "lng": 2.1695417
          },
          "viewport": {
            "northeast": {
              "lat": 41.40881393029149,
              "lng": 2.170837030291502
            },
            "southwest": {
              "lat": 41.40611596970849,
              "lng": 2.168139069708499
            }
          }
        },
        "name": "Teatre Gaud\\u00ed Barcelona",
        "photos": [
          {
            "height": 3024,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114118451392288609524/photos\\">Pako Valera</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJNXGXncaipBIRFcKUoG7Oj9Y",
        "rating": 4.3,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Sant Antoni Maria Claret, 120, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.39797799999999,
            "lng": 2.1701381
          },
          "viewport": {
            "northeast": {
              "lat": 41.3992910302915,
              "lng": 2.171534680291502
            },
            "southwest": {
              "lat": 41.3965930697085,
              "lng": 2.168836719708498
            }
          }
        },
        "name": "Hotel Easysleep Gaud\\u00ed",
        "photos": [
          {
            "height": 2976,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/100854084795632515753/photos\\">Nicholas Gray</a>"
            ],
            "width": 2976
          }
        ],
        "place_id": "ChIJk14XeumipBIRcCiLW27sMNE",
        "rating": 4,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Val\\u00e8ncia, 347, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.40499330000001,
            "lng": 2.1705953
          },
          "viewport": {
            "northeast": {
              "lat": 41.4063199302915,
              "lng": 2.171973830291502
            },
            "southwest": {
              "lat": 41.4036219697085,
              "lng": 2.169275869708498
            }
          }
        },
        "name": "Barcelona apartments Gaudi Flats",
        "place_id": "ChIJDdBG7MOipBIRi2s6YJmgVJs",
        "rating": 4.1,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de C\\u00f2rsega, 523, Barcelona"
      }
    ]
  },
  "Monuments": {
    "results": [
      {
        "geometry": {
          "location": {
            "lat": 41.3985565,
            "lng": 2.1667321
          },
          "viewport": {
            "northeast": {
              "lat": 41.3998952302915,
              "lng": 2.168085680291502
            },
            "southwest": {
              "lat": 41.3971972697085,
              "lng": 2.165387719708498
            }
          }
        },
        "name": "Monument a Narcis Monturiol",
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/107743002301134664494/photos\\">Javier Maestro</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJOxZL_OqipBIRunnN4p5OeNY",
        "rating": 4,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Avinguda Diagonal, 394, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3939082,
            "lng": 2.1622556
          },
          "viewport": {
            "northeast": {
              "lat": 41.39528138029149,
              "lng": 2.163705130291502
            },
            "southwest": {
              "lat": 41.39258341970849,
              "lng": 2.161007169708498
            }
          }
        },
        "name": "Monument Hotel",
        "photos": [
          {
            "height": 1350,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/104340260228794333207/photos\\">Monument Hotel</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJw-1RB5OipBIR0T-aKnKSAIA",
        "rating": 4.8,
        "types": [
          "lodging",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Passeig de Gr\\u00e0cia, 75, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3949095,
            "lng": 2.1756316
          },
          "viewport": {
            "northeast": {
              "lat": 41.3962584802915,
              "lng": 2.176980580291502
            },
            "southwest": {
              "lat": 41.3935605197085,
              "lng": 2.174282619708498
            }
          }
        },
        "name": "Monument al Doctor Bartomeu Robert",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3120,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/118038617267328065114/photos\\">Juan Antonio Silvestre</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJJ82w6eWipBIRA2dsQq2apwg",
        "rating": 3.8,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4042174,
            "lng": 2.163885399999999
          },
          "viewport": {
            "northeast": {
              "lat": 41.4053844802915,
              "lng": 2.164957680291502
            },
            "southwest": {
              "lat": 41.4026865197085,
              "lng": 2.162259719708498
            }
          }
        },
        "name": "Monument a Clav\\u00e9",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3642,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/114118451392288609524/photos\\">Pako Valera</a>"
            ],
            "width": 4874
          }
        ],
        "place_id": "ChIJZYDucr-ipBIROjfOquW-QQw",
        "rating": 3,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Passeig de Sant Joan, 200, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3900853,
            "lng": 2.1865555
          },
          "viewport": {
            "northeast": {
              "lat": 41.39162635,
              "lng": 2.18972485
            },
            "southwest": {
              "lat": 41.38546215,
              "lng": 2.185499049999999
            }
          }
        },
        "name": "Cascada Monumental",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1561,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/100317679938414361643/photos\\">Saulius Lag</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJx8AG1wKjpBIRrOvo5DT0OmI",
        "rating": 4.7,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Parc de la Ciutadella, Passeig de Picasso, 21, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3862735,
            "lng": 2.1700949
          },
          "viewport": {
            "northeast": {
              "lat": 41.3875316802915,
              "lng": 2.170997230291502
            },
            "southwest": {
              "lat": 41.3848337197085,
              "lng": 2.168299269708499
            }
          }
        },
        "name": "Monument a Francesc Maci\\u00e0",
        "photos": [
          {
            "height": 1152,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111114946461226307520/photos\\">Mehmet ABACI</a>"
            ],
            "width": 2048
          }
        ],
        "place_id": "ChIJXcixUfGipBIR1QYwWuPHlfw",
        "rating": 4.1,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Pla\\u00e7a de Catalunya, 4, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.390884,
            "lng": 2.17751
          },
          "viewport": {
            "northeast": {
              "lat": 41.3922329802915,
              "lng": 2.178858980291502
            },
            "southwest": {
              "lat": 41.3895350197085,
              "lng": 2.176161019708498
            }
          }
        },
        "name": "Est\\u00e0tua de Rafael Casanova",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2340,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113699083514649562827/photos\\">Francesc Perez gomez</a>"
            ],
            "width": 4160
          }
        ],
        "place_id": "ChIJVcRo0uSipBIRRTsdSNRdg50",
        "rating": 4.1,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Ronda de Sant Pere, 41, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3895848,
            "lng": 2.1788207
          },
          "viewport": {
            "northeast": {
              "lat": 41.39099168029149,
              "lng": 2.180267480291502
            },
            "southwest": {
              "lat": 41.3882937197085,
              "lng": 2.177569519708498
            }
          }
        },
        "name": "Parr\\u00f2quia de Sant Pere de les Puel\\u00b7les",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 854,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/113513671520638071616/photos\\">Alex Edo</a>"
            ],
            "width": 2000
          }
        ],
        "place_id": "ChIJj9FBW_uipBIR_TimKi61stY",
        "rating": 4.6,
        "types": [
          "church",
          "place_of_worship",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de Llu\\u00eds el Piad\\u00f3s, 1, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3833257,
            "lng": 2.1719003
          },
          "viewport": {
            "northeast": {
              "lat": 41.3845879302915,
              "lng": 2.173317080291502
            },
            "southwest": {
              "lat": 41.3818899697085,
              "lng": 2.170619119708498
            }
          }
        },
        "name": "Palau Moja",
        "photos": [
          {
            "height": 948,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110338747204138083945/photos\\">Ivan Dentella</a>"
            ],
            "width": 1280
          }
        ],
        "place_id": "ChIJs8XnVfaipBIRjdDYccZAW0g",
        "rating": 4.3,
        "types": [
          "premise",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer de la Portaferrissa, 1, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3836421,
            "lng": 2.1758658
          },
          "viewport": {
            "northeast": {
              "lat": 41.38499108029151,
              "lng": 2.177214780291502
            },
            "southwest": {
              "lat": 41.38229311970851,
              "lng": 2.174516819708498
            }
          }
        },
        "name": "Monument als Herois del 1809",
        "photos": [
          {
            "height": 2980,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101796861001134206408/photos\\">Darryl Hobbins</a>"
            ],
            "width": 3973
          }
        ],
        "place_id": "ChIJzd5WtvmipBIRNN4hfaweE2A",
        "rating": 3.5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carrer del Bisbe, 10, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.399148,
            "lng": 2.1699476
          },
          "viewport": {
            "northeast": {
              "lat": 41.40049698029149,
              "lng": 2.171296580291502
            },
            "southwest": {
              "lat": 41.3977990197085,
              "lng": 2.168598619708498
            }
          }
        },
        "name": "Moss\\u00e8n Jacint Verdaguer",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2988,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/108712643562152205268/photos\\">mehmet ali alkan</a>"
            ],
            "width": 5312
          }
        ],
        "place_id": "ChIJy6_hjumipBIRSI9DOBHSw5o",
        "rating": 3.8,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Pla\\u00e7a de Moss\\u00e8n Jacint Verdaguer, s/n, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3758075,
            "lng": 2.1777689
          },
          "viewport": {
            "northeast": {
              "lat": 41.3771564802915,
              "lng": 2.179117880291502
            },
            "southwest": {
              "lat": 41.3744585197085,
              "lng": 2.176419919708498
            }
          }
        },
        "name": "Columbus Monument, Barcelona",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 3672,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/117393885890286071546/photos\\">roberto panzi</a>"
            ],
            "width": 4896
          }
        ],
        "place_id": "ChIJSwC911aipBIRt2zr380K3Co",
        "rating": 4.3,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Pla\\u00e7a Portal de la pau, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.38205480000001,
            "lng": 2.1770292
          },
          "viewport": {
            "northeast": {
              "lat": 41.3834074802915,
              "lng": 2.178382380291502
            },
            "southwest": {
              "lat": 41.3807095197085,
              "lng": 2.175684419708498
            }
          }
        },
        "name": "Monumento a los Castellers",
        "photos": [
          {
            "height": 2160,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110510793650565656230/photos\\">RICARDO MERAT</a>"
            ],
            "width": 3840
          }
        ],
        "place_id": "ChIJX-hAi_iipBIRlWSnK0O6Y38",
        "rating": 3.6,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Pla\\u00e7a de Sant Miquel, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.4001013,
            "lng": 2.1812552
          },
          "viewport": {
            "northeast": {
              "lat": 41.4012517302915,
              "lng": 2.182746380291502
            },
            "southwest": {
              "lat": 41.3985537697085,
              "lng": 2.180048419708498
            }
          }
        },
        "name": "Monumental Bullring of Barcelona",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 616,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/111632263198459958035/photos\\">Ralf Liebegott</a>"
            ],
            "width": 1024
          }
        ],
        "place_id": "ChIJx8yWmuCipBIRmGHSaNmc5l8",
        "rating": 3.6,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Gran Via de les Corts Catalanes, 749, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.36796830000001,
            "lng": 2.1686943
          },
          "viewport": {
            "northeast": {
              "lat": 41.3693186802915,
              "lng": 2.170034880291502
            },
            "southwest": {
              "lat": 41.3666207197085,
              "lng": 2.167336919708498
            }
          }
        },
        "name": "Monument a La Sardana",
        "photos": [
          {
            "height": 2448,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/108487834878465080165/photos\\">Louis Villedieu</a>"
            ],
            "width": 3264
          }
        ],
        "place_id": "ChIJgwxsmUOipBIRvF35KucSglY",
        "rating": 4.2,
        "types": [
          "museum",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Carretera de Montju\\u00efc, 42, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.389254,
            "lng": 2.168069
          },
          "viewport": {
            "northeast": {
              "lat": 41.3905692802915,
              "lng": 2.169485280291502
            },
            "southwest": {
              "lat": 41.3878713197085,
              "lng": 2.166787319708498
            }
          }
        },
        "name": "Monument al Llibre",
        "photos": [
          {
            "height": 2160,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110510793650565656230/photos\\">RICARDO MERAT</a>"
            ],
            "width": 3840
          }
        ],
        "place_id": "ChIJRwemRvKipBIRGoY21F1ki6c",
        "rating": 4,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Gran Via de les Corts Catalanes, 634, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3910524,
            "lng": 2.180644899999999
          },
          "viewport": {
            "northeast": {
              "lat": 41.39267948029149,
              "lng": 2.181953680291501
            },
            "southwest": {
              "lat": 41.3899815197085,
              "lng": 2.179255719708498
            }
          }
        },
        "name": "Arco de Triunfo de Barcelona",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2642,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/101796861001134206408/photos\\">Darryl Hobbins</a>"
            ],
            "width": 3523
          }
        ],
        "place_id": "ChIJoXZqNuOipBIRsZU39a8r_qk",
        "rating": 4.5,
        "types": [
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Passeig de Llu\\u00eds Companys, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3914803,
            "lng": 2.1649813
          },
          "viewport": {
            "northeast": {
              "lat": 41.3929438802915,
              "lng": 2.166455330291502
            },
            "southwest": {
              "lat": 41.3902459197085,
              "lng": 2.163757369708498
            }
          }
        },
        "name": "Casa Amatller",
        "opening_hours": {
          "open_now": false,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 1440,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/116744585688893559884/photos\\">Anton Lamoff</a>"
            ],
            "width": 2094
          }
        ],
        "place_id": "ChIJdTx2T-2ipBIRQM8Gwarzfik",
        "rating": 4.5,
        "types": [
          "museum",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Passeig de Gr\\u00e0cia, 41, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3838871,
            "lng": 2.1820711
          },
          "viewport": {
            "northeast": {
              "lat": 41.38515333029149,
              "lng": 2.183454430291502
            },
            "southwest": {
              "lat": 41.3824553697085,
              "lng": 2.180756469708498
            }
          }
        },
        "name": "Basilica of Santa Maria del Mar",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 965,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/115414097683053387354/photos\\">Bas\\u00edlica de Santa Maria del Mar</a>"
            ],
            "width": 966
          }
        ],
        "place_id": "ChIJV6QNXv6ipBIRYOHAH1yUvLQ",
        "rating": 4.6,
        "types": [
          "church",
          "place_of_worship",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Pla\\u00e7a de Santa Maria, 1, Barcelona"
      },
      {
        "geometry": {
          "location": {
            "lat": 41.3938255,
            "lng": 2.1621774
          },
          "viewport": {
            "northeast": {
              "lat": 41.39525843029151,
              "lng": 2.163642380291502
            },
            "southwest": {
              "lat": 41.39256046970851,
              "lng": 2.160944419708498
            }
          }
        },
        "name": "Oria",
        "opening_hours": {
          "open_now": true,
          "weekday_text": []
        },
        "photos": [
          {
            "height": 2268,
            "html_attributions": [
              "<a href=\\"https://maps.google.com/maps/contrib/110326414720018839779/photos\\">Joe Vraneza</a>"
            ],
            "width": 4032
          }
        ],
        "place_id": "ChIJGat6qpOipBIRvfTpqFHcB68",
        "rating": 4.8,
        "types": [
          "restaurant",
          "food",
          "point_of_interest",
          "establishment"
        ],
        "vicinity": "Passeig de Gr\\u00e0cia, 75, Barcelona"
      }
    ]
  }
}
""",

    }
)
