import sys

from django.test import TestCase
from unittest import skip, skipIf

from .london_tourist_spots import london_tourist_spots
from tl_trailer.utilities.search_helper import SearchHelper
from tl_trailer.models import Trail, Zone

from . import test_level

search_helper = SearchHelper()


@skip("Deferred")
class LondonSearchTest(TestCase):

    def __init__(self, context):
        TestCase.__init__(self, context)

    @staticmethod
    def setUpClass():
        TestCase.setUpClass()
        search_helper.set_data(london_tourist_spots)
        print(file=sys.stderr)
        sys.stderr.write('LondonSearchTest: ')
        pass

    @skipIf(test_level > 0, "Suppressing tests which consume Google API quota")
    def test_locality_search(self):
        search_helper.locality_search()

    @skipIf(test_level > 0, "Suppressing tests which consume Google API quota")
    def test_single_keyword_search(self):
        search_helper.single_keyword_search()

    def test_builder(self):
        built_trail = search_helper.build_trail()
        trail_id = built_trail.id
        (retrieved_trail,) = Trail.objects.filter(id=trail_id)
        self.assertIsNot(retrieved_trail.id, 1)
        self.assertIs(3, len(retrieved_trail.zone_set.all()))

    def test_radius(self):
        search_helper.build_trail(min_radius_metres=20.0)
        (traf_sq,) = Zone.objects.filter(name='Trafalgar Square')
        # We expect the radius of Trafalgar Square to be between
        # 75 and 125 metres
        self.assertGreaterEqual(traf_sq.radius_metres, 75.0)
        self.assertLessEqual(traf_sq.radius_metres, 125.0)

    @staticmethod
    def tearDownClass():
        TestCase.tearDownClass()
        search_helper.set_data(None, 'london_tourist_spots')
        print(file=sys.stderr)
