import sys

from django.test import TestCase
from unittest import skip, skipIf

from tl_trailer.utilities.search_helper import SearchHelper
from tl_trailer.models import Trail

from . import test_level

from .barcelona_culture import barcelona_culture

search_helper = SearchHelper()


@skip("Deferred")
class BarcelonaSearchTest(TestCase):

    def __init__(self, context):
        TestCase.__init__(self, context)

    @staticmethod
    def setUpClass():
        TestCase.setUpClass()
        search_helper.set_data(barcelona_culture)
        print(file=sys.stderr)
        sys.stderr.write('BarcelonaSearchTest: ')

    @skipIf(test_level > 0, "Suppressing tests which consume Google API quota")
    def test_locality_search(self):
        search_helper.locality_search()

    @skipIf(test_level > 0, "Suppressing tests which consume Google API quota")
    def test_single_keyword_search(self):
        search_helper.single_keyword_search()

    def test_builder(self):
        built_trail = search_helper.build_trail()
        trail_id = built_trail.id
        (retrieved_trail,) = Trail.objects.filter(id=trail_id)
        self.assertIsNot(retrieved_trail.id, 1)
        self.assertIs(10, len(retrieved_trail.zone_set.all()))

    @staticmethod
    def tearDownClass():
        TestCase.tearDownClass()
        search_helper.set_data(None,'barcelona_culture')
        print(file=sys.stderr)
