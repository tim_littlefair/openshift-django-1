import sys

from django.test import TestCase
from unittest import skip

from tl_trailer.utilities.example_data import ExampleData


@skip("Deferred")
class TrailViewTest(TestCase):

    def __init__(self, context):
        TestCase.__init__(self, context)

    @staticmethod
    def setUpClass():
        TestCase.setUpClass()
        print(file=sys.stderr)
        sys.stderr.write('TrailViewTest: ')

    def setUp(self):
        self.example_data = ExampleData()
        self.test_trail_id = self.example_data.barcelona_trail.id
        self.url_get_params = '?type=Trail&id=%d' % (self.test_trail_id,)

    def test_trail_layout(self):
        urlpath = '/trailer' + self.url_get_params
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)

    def test_trail_table(self):
        urlpath = '/trailer/table' + self.url_get_params
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)

    def test_trail_map(self):
        urlpath = '/trailer/map' + self.url_get_params
        response = self.client.get(urlpath)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Attempting to load map with bounds')

    def tearDown(self):
        pass

    @staticmethod
    def tearDownClass():
        TestCase.tearDownClass()
        print(file=sys.stderr)
