from tl_trailer.utilities.map_utils import get_map_bounds, _object_bounds
from tl_trailer.models import Group, Trail, Zone


REQPARAM_TYPE = 'type'
REQPARAM_ID = 'id'
REQPARAM_LHMODE = 'lhmode'
REQPARAM_RHMODE = 'rhmode'


def urlparams_to_item_and_bounds(request):
    params = request.GET
    item = None
    children = []
    if REQPARAM_TYPE in params and REQPARAM_ID in params:
        type_ = params[REQPARAM_TYPE]
        id_ = int(params[REQPARAM_ID])
        if type_ == 'Group':
            item = Group.objects.get(pk=id_)
            children += item.subgroups.all()
            children += item.trails.all()
        elif type_ == 'Trail':
            item = Trail.objects.get(pk=id_)
            children += item.zone_set.all()
        elif type_ == 'Zone':
            item = Zone.objects.get(pk=id_)
            children = []
        map_bounds = get_map_bounds(type_, id_)
    else:
        map_bounds = -80.0, -180.0, +80.0, +180.0
    for c in children:
        c.bounds = visible_bounds(map_bounds,_object_bounds(c))
    return item, children, map_bounds


def longitude_range(probably_high, probably_low):
    retval = probably_high - probably_low
    if retval < 0:
        # This can only happen for longitude,
        # in a case where the range spans
        # the +/-180 degree line of latitude
        retval += 360
    return retval


def visible_bounds(map_bounds, object_bounds, min_size_factor=0.15):
    map_latitude_range = map_bounds[2] - map_bounds[0]
    map_longitude_range = longitude_range(map_bounds[3], map_bounds[1])
    object_latitude_range = object_bounds[2] - object_bounds[0]
    object_longitude_range = longitude_range(
        object_bounds[3], object_bounds[1])
    min_display_latitude_range = map_latitude_range*min_size_factor
    min_display_longitude_range = map_longitude_range*min_size_factor
    latitude_magnification = max(
        1, min_display_latitude_range / object_latitude_range
    )
    longitude_magnification = max(
        1, min_display_longitude_range / object_longitude_range
    )
    magnification = max(latitude_magnification, longitude_magnification)
    latitude_expansion = 0.5 * (magnification-1) * object_latitude_range
    longitude_expansion = 0.5 * (magnification-1) * object_longitude_range
    magnified_bounds = (
        object_bounds[0] - latitude_expansion,
        object_bounds[1] - longitude_expansion,
        object_bounds[2] + latitude_expansion,
        object_bounds[3] + longitude_expansion,
    )
    return magnified_bounds
