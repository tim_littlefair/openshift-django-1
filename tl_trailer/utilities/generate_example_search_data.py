import io
import time

generated_txt = None
generated_filename = None


def _print_to_file(lines=''):
    global generated_txt
    KEYS_TO_FILTER = (
        'id', 'next_page_token',
        'reference', 'photo_reference',
        'scope', 'icon',
    )
    if(generated_txt):
        for line in lines.split("\n"):
            filter_line = False
            for k in KEYS_TO_FILTER:
                if '"'+k+'":' in line:
                    filter_line = True
                    break
            if filter_line is False:
                print(line, file=generated_txt)
    # print(generated_txt.getvalue())


def open_file():
    global generated_txt, generated_filename
    generated_txt = io.StringIO()
    _print_to_file('# Generated '+time.ctime())
    _print_to_file(
        '# pylint:disable=line-too-long,too-many-lines,missing-docstring'
    )
    _print_to_file()
    _print_to_file(
        'from tl_trailer.utilities.tuple_types import ExampleSearchData'
    )
    _print_to_file()


def open_example_object(london_tourist_spots):
    _print_to_file('london_tourist_spots = ExampleSearchData(')
    _print_to_file("    '%s'," % (
        london_tourist_spots.locality.replace('\\', '\\\\'),)
    )
    _print_to_file("    '%s'," % (
        london_tourist_spots.keywords.replace('\\', '\\\\'),)
    )
    _print_to_file('    {')


def append_json_result(varname, url, result_json):
    _print_to_file("        '%s':" % (varname,))
    _print_to_file()
    _print_to_file("        # " + url)
    _print_to_file('        """')
    _print_to_file(result_json.replace('\\', '\\\\',))
    _print_to_file('""",')
    _print_to_file()


def close_file(basename):
    global generated_txt
    if basename:
        _print_to_file('    }')
        _print_to_file(')')
        gen_file = open(
            'tl_trailer/tests/' + basename + '.txt',
            mode='w', encoding='utf-8'
        )
        gen_file.write(generated_txt.getvalue())
        gen_file.close()
    generated_txt = None
