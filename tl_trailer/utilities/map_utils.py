from tl_trailer.models import Zone, Trail, Group

_RADIUS_OF_EARTH_KM = 6371

class_map = {'Zone': Zone, 'Trail': Trail, 'Group': Group}


def get_map_bounds(type_, id_):
    object_ = class_map[type_].objects.get(pk=id_)
    return _object_bounds(object_)


def _object_bounds(object_, previous_bounds=None):
    if isinstance(object_, Zone):
        return (
            object_.southern_latitude, object_.western_longitude,
            object_.northern_latitude, object_.eastern_longitude
        )
    elif isinstance(object_, Trail):
        zones = object_.zone_set.all()
        bounds = _object_bounds(zones[0])
        for zone in zones[1:]:
            bounds = _combine_bounds(bounds, _object_bounds(zone))
        return bounds
    elif isinstance(object_, Group):
        members = []
        members += object_.trails.all()
        members += object_.subgroups.all()
        bounds = _object_bounds(members[0])
        for member in members[1:]:
            bounds = _combine_bounds(bounds, _object_bounds(member))
        return bounds
    else:
        raise RuntimeError("Unexpected object type")


def _combine_bounds(initial, to_add):
    combined = [None, None, None, None]
    combined[0] = min(initial[0], to_add[0])
    combined[2] = max(initial[2], to_add[2])
    if(initial[1] < initial[3]):
        # the initial bounds stradle the +180/-180
        # line of longitude in the Pacific
        # This reverses the normal selection
        # logic
        combined[1] = min(initial[1], to_add[1])
        combined[3] = max(initial[3], to_add[3])
    else:
        # Normal situation
        combined[1] = max(initial[1], to_add[1])
        combined[3] = min(initial[3], to_add[3])
    return tuple(combined)
