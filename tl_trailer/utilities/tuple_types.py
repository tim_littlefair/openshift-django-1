from collections import namedtuple

ExampleSearchData = namedtuple(
    'ExampleData',
    (
        'locality',
        'keywords',
        'json_response_set',
    )
)
