from tl_trailer.models import Group

from tl_trailer.tests.london_tourist_spots import london_tourist_spots
from tl_trailer.tests.edinburgh_entertainment import edinburgh_entertainment
from tl_trailer.tests.barcelona_culture import barcelona_culture

from .search_helper import SearchHelper


class ExampleData:
    def __init__(self):
        search_helper = SearchHelper()
        search_helper.set_data(london_tourist_spots)
        self.london_trail = search_helper.build_trail()
        self.london_trail.description = (
            'This example trail shows the effect of ' +
            'explicitly searching for the following London ' +
            'landmarks: Trafalgar Square, Tower Bridge, Euston Station.'
        )
        self.london_trail.save()
        search_helper.set_data(edinburgh_entertainment)
        self.edinburgh_trail = search_helper.build_trail()
        self.edinburgh_trail.save()
        search_helper.set_data(barcelona_culture)
        self.barcelona_trail = search_helper.build_trail()
        self.barcelona_trail.save()
        self.uk_group = Group()
        self.uk_group.name = 'UK'
        self.uk_group.save()
        self.euro_group = Group()
        self.euro_group.name = 'Europe'
        self.euro_group.notes = (
            'This example group has one nested subgroup ' +
            'and one directly attached trail.'
        )
        self.euro_group.save()
        self.uk_group.trails.add(self.london_trail)
        self.uk_group.trails.add(self.edinburgh_trail)
        self.uk_group.save()
        self.euro_group.subgroups.add(self.uk_group)
        self.euro_group.trails.add(self.barcelona_trail)
        self.euro_group.save()
