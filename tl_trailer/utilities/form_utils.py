import collections

from tl_trailer.models import Group, Trail, Zone

FormItem = collections.namedtuple('FormItem', 'type value extra_attributes')


def summary_form_items(object_):
    form_items = []
    if isinstance(object_, Group):
        form_items += [
            FormItem('label', 'Name', None),
            FormItem('single_line_input', object_.name, 'id="name"'),
            FormItem('label', 'Notes', None),
            FormItem('multi_line_input', object_.notes, 'id="notes" rows="3"'),
            # FormItem('checkbox', 'use_curloc', 'Current location'),
            # FormItem('label', 'Keywords', None),
            # FormItem('multi_line_input', 'keywords', 'rows="12"'),
            # FormItem(
            #     'checkbox', 'bookmark_consent',
            #     'Create a bookmark for my new trail'
            # )
            # FormItem('submit', 'create_trail', 'Create a new trail'),
        ]
    elif isinstance(object_, Trail):
        form_items += [
            FormItem('label', 'Locality', None),
            FormItem('single_line_input', object_.locality, 'id="locality"'),
            FormItem('label', 'Description', None),
            FormItem('multi_line_input', object_.description, 'id="description" rows="3"'),
            FormItem('label', 'Expiry date', None),
            FormItem('single_line_input', object_.expiry_date, 'id="expdate"'),
        ]
    elif isinstance(object_, Zone):
        form_items += [
            FormItem('label', 'Name', None),
            FormItem('single_line_input', object_.name, 'id="name"'),
            FormItem('label', 'Latitude', None),
            FormItem('single_line_input', object_.latitude, 'id="latitude"'),
            FormItem('label', 'Longitude', None),
            FormItem('single_line_input', object_.longitude, 'id="longitude"'),
            FormItem('label', 'Radius (metres)', None),
            FormItem('single_line_input', object_.radius_metres, 'id="radius_metres"'),
        ]
    return form_items


def table_form_items(object_):
    form_items = []
    if isinstance(object_, Group):
        if len(object_.trails.all()) > 0:
            form_items += [FormItem('label', 'Trails', None)]
            for trail in object_.trails.all():
                action = """href="/trailer/layout_redirector?type=Trail&id=%d" """ % (
                    trail.id,
                )
                form_items += [FormItem('link', trail.locality, action)]
        if len(object_.subgroups.all()) > 0:
            form_items += [FormItem('label', 'Subgroups', None)]
            for group in object_.subgroups.all():
                action = """href="/trailer/layout_redirector?type=Group&id=%d" """ % (
                    group.id,
                )
                form_items += [FormItem('link', group.name, action)]
    elif isinstance(object_, Trail):
        for zone in object_.zone_set.all():
            action = """href="/trailer/layout_redirector?type=Zone&id=%d" """ % (
                zone.id,
            )
            form_items += [FormItem('link', zone.name, action)]
    return form_items


def _wrap_value(value):
    return ' value="%s"' % (value,)
