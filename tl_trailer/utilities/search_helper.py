import requests
import urllib.parse
import json

from tl_trailer.builder import Director, DefaultBuilder

from .environment import google_api_key
from .generate_example_search_data import (
    open_file, open_example_object, append_json_result, close_file
)

GMAPS_URL_PREFIX = "https://maps.googleapis.com/maps/api"
TEXT_SEARCH_URL = GMAPS_URL_PREFIX + "/place/textsearch/json"
NEARBY_SEARCH_URL = GMAPS_URL_PREFIX + "/place/nearbysearch/json"


class SearchHelper:
    @staticmethod
    def set_data(ex_data, cache_filename=None):
        global london_tourist_spots
        london_tourist_spots = ex_data
        if ex_data :
            open_file()
        elif ex_data is None:
            close_file(cache_filename)

    def locality_search(self):
        open_example_object(london_tourist_spots)
        url = "%s?key=%s&query=%s" % (
            TEXT_SEARCH_URL,
            google_api_key,
            urllib.parse.quote_plus(london_tourist_spots.locality, 'utf-8')
        )
        response = requests.get(url)
        self._dump_url_and_response(
            'locality_result', url,
            json.loads(response.text)
        )

    def single_keyword_search(self):
        locality_result = json.loads(
            london_tourist_spots.json_response_set['locality_result']
        )
        locality_location = (
            locality_result['results'][0]['geometry']['location']
        )
        url_prefix = "%s?key=%s&location=%.7f%s%.7f&radius=25000" % (
            NEARBY_SEARCH_URL,
            google_api_key,
            locality_location['lat'],
            urllib.parse.quote_plus(',', 'utf-8'),
            locality_location['lng']
        )
        MAX_KEYWORD_RESULTS = 100
        keyword_results = {}
        for keyword in str.split(london_tourist_spots.keywords, '\\n'):
            keyword = keyword.strip()
            if(keyword):
                url = (
                    url_prefix + "&keyword=" +
                    urllib.parse.quote_plus(keyword)
                )
                response = requests.get(url)
                results = json.loads(response.text)['results']
                if len(results) > MAX_KEYWORD_RESULTS:
                    results = results[0:MAX_KEYWORD_RESULTS]
                keyword_results[keyword] = {}
                keyword_results[keyword]['results'] = results
        self._dump_url_and_response('keyword_results', url, keyword_results)

    def build_trail(self, max_results=10, min_radius_metres=100.0):
        builder = DefaultBuilder(min_radius_metres=min_radius_metres)
        director = Director(builder)
        trail = director.construct(
            london_tourist_spots.json_response_set['locality_result'],
            london_tourist_spots.json_response_set['keyword_results'],
            max_results
        )
        return trail

    def _dump_url_and_response(self, varname, url, response_dom):
        result = json.dumps(response_dom, indent='  ', sort_keys=True)
        append_json_result(
            varname,
            url.replace(google_api_key, '_redacted_'),
            result
        )
