import os
import urllib

from django.shortcuts import render

from tl_trailer.models import Trail
from tl_trailer.utilities.url_utils import urlparams_to_item_and_bounds
from tl_trailer.utilities.form_utils import (
    FormItem, summary_form_items, table_form_items  # , details_form_items
)


def header(request):
    return render(request, 'html/other/header.html', {
        'prefix': '/trailer',
        'title': 'Trailer is under reconstruction',
        'auth_status': 'Not logged in',
        'auth_op_label': 'Log in',
        'auth_url': 'login',
    })


def footer(request):
    return render(request, 'html/other/footer.html', {
        'prefix': '/trailer',
        'appver': '0.0.0 (2017-05-20)',
    })


def navigation(request):
    navigation_items = [
        # {'text': 'Featured trails', 'url': 'featured'},
        {'text': 'Example trails', 'url': 'examples'},
        # {'text': 'Create a new trail', 'url': 'create'},
    ]
    return render(request, 'html/panels/navigation.html', {
        'prefix': '/trailer',
        'navigation_items': navigation_items,
    })


def short_welcome(request):
    return render(request, 'html/panels/short_welcome.html', {
        'prefix': '/trailer',
    })


# The word 'map' is a built in symbol in Python, so we
# use the equivalent Swedish word to avoid redefining it.
def karta(request):
    _, children, map_bounds = urlparams_to_item_and_bounds(request)
    list_items = children
    js_bounds_literal = (
        '{ south: %f, west: %f, north: %f, east: %f }' % map_bounds
    )
    return render(request, 'html/panels/map.html', {
        'google_api_key': os.getenv('GOOGLE_API_KEY_JS_MAPS'),
        'prefix': '/trailer',
        'bounds': js_bounds_literal,
        'list_items': list_items
    })


def summary(request):
    item, _, map_bounds = urlparams_to_item_and_bounds(request)
    form_items = summary_form_items(item)
    if False and isinstance(item, Trail):
        js_bounds_literal = (
            '{ south: %f, west: %f, north: %f, east: %f }' % map_bounds
        )
        print(js_bounds_literal)
        link_format = 'href="/trailer/layout_redirector?type=Trail&bounds=%s"'
        action = link_format % (
            urllib.parse.quote(js_bounds_literal)
        )
        form_items += [FormItem('link', 'overlapping trails', action)]
    return render(request, 'html/panels/form.html', {
        'prefix': '/trailer',
        'form_items': form_items,
        'google_api_key': os.getenv('GOOGLE_API_KEY_JS_MAPS'),
    })


def table(request):
    item, _, _ = urlparams_to_item_and_bounds(request)
    form_items = table_form_items(item)
    return render(request, 'html/panels/form.html', {
        'prefix': '/trailer',
        'form_items': form_items,
        'google_api_key': os.getenv('GOOGLE_API_KEY_JS_MAPS'),
    })


def details(request):
    header_row = ('Regions', '',)
    rows = [
        ('North America', ''),
        ('United Kingdom', ''),
        ('Scandinavia', ''),
        ('Southern Europe', ''),
        ('Asia', ''),
        ('Australia', ''),
        ('New Zealand', ''),
    ]
    return render(request, 'html/panels/table.html', {
        'prefix': '/trailer',
        'header_row': header_row,
        'rows': rows,
        'google_api_key': os.getenv('GOOGLE_API_KEY_JS_MAPS'),
    })


def creation_instructions(request):
    return render(request, 'html/panels/creation_instructions.html', {
        'prefix': '/trailer',
    })


def creation_form(request):
    form_items = [
        FormItem('label', 'Locality', None),
        FormItem('single_line_input', 'locality', None),
        # FormItem('checkbox', 'use_curloc', 'Current location'),
        FormItem('label', 'Keywords', None),
        FormItem('multi_line_input', 'keywords', 'rows="12"'),
        # FormItem(
        #     'checkbox', 'bookmark_consent',
        #     'Create a bookmark for my new trail'
        # )
        FormItem('submit', 'create_trail', 'Create a new trail'),
    ]
    return render(request, 'html/panels/form.html', {
        'prefix': '/trailer',
        'form_items': form_items,
        'google_api_key': os.getenv('GOOGLE_API_KEY_JS_MAPS'),
    })


