from .layouts import dbobject, featured, create, layout_redirector
from .panels import header, footer, navigation, short_welcome
from .panels import karta, summary, table, details
from .panels import creation_instructions, creation_form
from .examples import examples


from django.shortcuts import render
from django.http import HttpResponse


def render_static(request):
    # This function has been created because of problems
    # accessing static files in OpenShift which could not
    # be reproduced in production.
    # We work around this for the time being by serving
    # these resources from the template directory, even
    # though there are no substitutions required.
    resource_path = request.path.replace('/trailer/', '')
    return render(request, resource_path)


def health(request):
    return HttpResponse("I'm OK if you're OK")


def under_reconstruction(request):
    return render(request, 'html/other/under_reconstruction.html', {})
