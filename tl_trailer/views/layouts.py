from django.shortcuts import render

from tl_trailer.utilities.url_utils import (
    REQPARAM_TYPE, REQPARAM_ID, REQPARAM_RHMODE
)


def dbobject(request):
    rhmode = 'summary_and_table'
    params = request.GET
    if REQPARAM_TYPE in params and REQPARAM_ID in params:
        url_params = '?type=%s&id=%s' % (
            params[REQPARAM_TYPE], params[REQPARAM_ID],
        )
    else:
        url_params = ''
        rhmode = 'table_only'
    if REQPARAM_RHMODE in params:
        rhmode = params[REQPARAM_RHMODE]
    return render(request, 'html/layouts/layout-flex.html', {
        'prefix': '/trailer',
        'rhmode': rhmode,
        'url_params': url_params
    })


def featured(request):
    return render(request, 'html/layouts/layout-flex.html', {
        'prefix': '/trailer',
        'rhmode': 'table_only',
        'url_params': ''
    })


def create(request):
    return render(request, 'html/layouts/layout-flex.html', {
        'prefix': '/trailer',
        'rhmode': 'instructions_and_form',
        'context': 'creation',
    })


def layout_redirector(request):
    return render(request, 'html/layouts/layout-redirector.html', {
        'prefix': '/trailer',
        'location': request.get_full_path().replace('/layout_redirector', ''),
    })
