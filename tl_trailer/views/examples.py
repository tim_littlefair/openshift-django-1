from django.shortcuts import render, redirect
import collections

from tl_trailer.utilities.example_data import ExampleData

"""
from tl_trailer.tests.barcelona_culture import barcelona_culture

from tl_trailer.tests.london_tourist_spots import (
    london_tourist_spots as london_example_data
)

from tl_trailer.tests.edinburgh_entertainment import (
    london_tourist_spots as edinburgh_example_data
)
"""


from tl_trailer.utilities.search_helper import SearchHelper


def examples(request):
    return example_group_layout(request)


def example_trail_layout(request):
    exdata = ExampleData()
    return redirect('/trailer?type=Trail&id=%d' % (exdata.london_trail.id, ))


def example_group_layout(request):
    exdata = ExampleData()
    return redirect('/trailer?type=Group&id=%d' % (exdata.euro_group.id, ))


def test_form(request):
    FormItem = collections.namedtuple('FormItem', 'type text extra')
    form_items = [
        FormItem('label', 'Test Locality', 'value="London"',),
        FormItem('single_line_input', 'locality', ' value="London"',),
        # FormItem('checkbox', 'use_curloc', 'Current location'),
        FormItem('label', 'Test Keywords', None, ),
        FormItem('multi_line_input', 'keywords', 'rows="12"',),
        # FormItem('checkbox', 'bookmark', 'Create bookmark for new trail'),
        FormItem('submit', 'create_trail', 'Create a new trail'),
        FormItem('javascript',  'console.log(locality.text);', None),
        FormItem('javascript',  'console.log(locality.text);', None),
        FormItem(
            'javascript',
            'keywords.text="Trafalgar Square\\nCamden Lock\\nTower Bridge\\n";',
            None
        ),
    ]
    return render(request, 'html/panels/form.html', {
        'prefix': '/trailer',
        'form_items': form_items,
    })


def test_panel(request):
    return render(request, 'html/layouts/layout-flex.html', {
        'prefix': '/trailer',
        'rhmode': 'instructions_and_form',
    })
