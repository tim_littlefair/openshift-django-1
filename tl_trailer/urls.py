from django.conf.urls import include, url
from django.conf.urls.static import static

from tl_project import settings


from . import views

trailer_urlpatterns = [
    # Default page
    url(r'^trailer$', views.featured),

    # Layouts (i.e. whole pages)
    url(r'^featured$', views.featured),
    url(r'^create$', views.create),

    # A redirector URL which can be linked to
    # from panels but will trigger a redisplay
    # of the whole page
    url(r'^layout_redirector', views.layout_redirector),


    # Panels (used for parts of pages)

    # Top of page
    url(r'^header$', views.header),

    # Left hand column
    url(r'^navigation$', views.navigation),
    url(r'^short_welcome$', views.short_welcome),

    # Middle column
    url(r'^map$', views.karta),

    # Right hand column (viewing)
    url(r'^summary$', views.summary),
    url(r'^table$', views.table),
    url(r'^details$', views.details),

    # Right handl column (creating)
    url(r'^creation_instructions$', views.creation_instructions),
    url(r'^creation_form$', views.creation_form),

    # Bottom of page
    url(r'^footer$', views.footer),

    url(r'^.*\.js$', views.render_static),

    # Tests
    url(r'^examples$', views.examples),

    # Catch-all
    url(r'^.*$', views.under_reconstruction),
]

urlpatterns = [
    # Default page
    url(r'^trailer$', views.dbobject),
    url(r'^trailer/', include(trailer_urlpatterns)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
