// Module-global variables
var geocoder = new google.maps.Geocoder();

var geocode_string = null;
var geocode_enabled = true;
var the_form;
var locality_input;
var latitute_input;
var longitude_input;
var description_input;

function errorMessage(message) {
    alert(message);
}

function debugMessage(message) {
    alert(message);
}

function setup_globals() {
    var dtl_iframe = document.getElementById("dtl_iframe")
    if(dtl_iframe!=null) {
    	locality_input = dtl_iframe.contentDocument.getElementById("locality");
    	latitude_input = dtl_iframe.contentDocument.getElementById("latitude");
    	longitude_input = dtl_iframe.contentDocument.getElementById("longitude");
    	description_input = dtl_iframe.contentDocument.getElementById("description");
    	handle_input = dtl_iframe.contentDocument.getElementById("handle")
    } else {
    	errorMessage("sg: dtl_iframe not found");
    }
}

function handle_input_field_geocode_response(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
		setup_globals();
    	first_result_location = results[0].geometry.location
    	if(latitude_input != null) { 
    		latitude_input.value = first_result_location.lat();
    	}
    	if(longitude_input != null) { 
    		longitude_input.value = first_result_location.lng();
    	}
     } else { 
		errorMessage("geocode failed with status "+ status);
	}
}

function send_input_field_geocode_request() {	
	setup_globals();
	geocode_string = locality_input.value
    geocoder.geocode( 
    	{ 'address': geocode_string }, 
        handle_input_field_geocode_response
    );
}


