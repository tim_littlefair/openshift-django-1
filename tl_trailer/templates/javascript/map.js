/* utilities.js */
/* Copyright 2011, Tim and Jack Littlefair */

// var geocoder = new google.maps.Geocoder();
var map;
var place_name_element;
var latitude_element;
var longitude_element;
var first_result_location;
var group_array = [];
var trail_array = [];
var zone_array = [];
var added_item_type;
var latitude_input;
var longitude_input;
var draggable_overlay;
var marker_image = new google.maps.MarkerImage(
	/* url */ "/static/annotation_zone.png",
	/* size */ null,
	/* origin */ null,
	/* anchor */ new google.maps.Point(10,10),
	/* scaledSize */ new google.maps.Size(20,20)
)

function createOverlay(centre,radius) {
}

function createGroupMarker(lat_min,long_min,lat_max,long_max) {
	deltaLat = 1.0;
	deltaLong = 1.0;
	var groupVertices = [ 
		new google.maps.LatLng(lat_max,long_max),
		new google.maps.LatLng(lat_min,long_max),
		new google.maps.LatLng(lat_min,long_min),
		new google.maps.LatLng(lat_max,long_min)
	];
	
	return new google.maps.Polygon({
		paths:groupVertices,
		fillColor: "#222222",
		fillOpacity: 0.2,
		strokeWeight: 1,
    	strokeColor: "#999999",
		strokeOpacity: 0.7,
		map: map
	});		
}

function createTrailMarker(lat_min,long_min,lat_max,long_max) {
	lat_mid = 0.5*(lat_min+lat_max)
	long_mid = 0.5*(long_min+long_max)
	var trailVertices = [ 
		new google.maps.LatLng(lat_min,long_mid),
		new google.maps.LatLng(lat_mid,long_min),
		new google.maps.LatLng(lat_max,long_mid),
		new google.maps.LatLng(lat_mid,long_max)
	];
	
	return new google.maps.Polygon({
		paths:trailVertices,
		fillColor: "#2222ee",
		fillOpacity: 0.2,
		strokeWeight: 1,
    	strokeColor: "#222299",
		strokeOpacity: 0.7,
		map: map
	});		
}

function createZoneMarker(lat,long,radius,draggable) {
	var overlay = new google.maps.Circle({
			center: new google.maps.LatLng(lat,long),
			radius: radius,
			fillColor: "#22ee22",
			fillOpacity: 0.2,
			strokeWeight: 1,
	    	strokeColor: "#229922",
			strokeOpacity: 0.7,
			map: map
	});
	if(draggable==true) {
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat,long),
			draggable: true,
			icon: marker_image,
			map: map
		});	
		draggable_overlay = overlay 
		google.maps.event.addListener(marker,"dragend",marker_dragged)
	} 
	else
	{
		draggable_overlay = null
	}
}


function add_group(id,lat_max,lat_min,long_max,long_min) {
	var group_details = [id,lat_max,lat_min,long_max,long_min];
    group_array.push(group_details); 
}
function add_trail(id,lat_max,lat_min,long_max,long_min) {
	var trail_details = [id,lat_max,lat_min,long_max,long_min];
	trail_array.push(trail_details);
}
function add_zone(id,lat,long,radius,draggable) {
	var zone_details = [id,lat,long,radius,draggable];
    zone_array.push(zone_details); 
}

function marker_dragged(event) {
	var oldRadius = draggable_overlay.radius;
	draggable_overlay.setMap(null);
	
	// If the map is displayed alongside the zone details
	// form, there should only be one marker and we want 
	// to make it draggable so that the details on the form
	// can be updated.
	place_name_element = document.getElementById("place_name");
	latitude_element = document.getElementById("latitude");
	longitude_element = document.getElementById("longitude");
    var frameParent = window.frameElement.parentElement
    var detailsFrame = frameParent.parentElement.children[3].firstChild
    var detailsDoc = detailsFrame.contentDocument
    latitude_input = detailsDoc.getElementById('latitude')
    longitude_input = detailsDoc.getElementById('longitude')	
    latitude_input.value=event.latLng.lat()
    longitude_input.value= event.latLng.lng()
    
	draggable_overlay = createOverlay(event.latLng,oldRadius)
}

function initialize_map(bounds) {
	map = new google.maps.Map(
		document.getElementById("map_canvas"), 
		{
			// zoom: zoom,
			// center: new google.maps.LatLng(latitude,longitude),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		    disableDefaultUI: true, // If not, the controls get drawn multiple times and look ugly
		}
	);
	map.fitBounds(bounds);
	// geocoder = new google.maps.Geocoder();
	
	while(group_array.length>0) {
		var gd = group_array[0];
		createGroupMarker(gd[1],gd[2],gd[3],gd[4]);
		group_array.shift();
	}
	while(trail_array.length>0) {
		var td = trail_array[0];
		createTrailMarker(td[1],td[2],td[3],td[4]);
		trail_array.shift();
	}
		
	while(zone_array.length>0) {
		var zd = zone_array[0];
		createZoneMarker(zd[1],zd[2],zd[3],zd[4]);
 		zone_array.shift();
	}
}

function handle_geocode_response(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
    	first_result_location = results[0].geometry.location 
    	document.forms["the_form"].latitude.value = first_result_location.lat();
    	document.forms["the_form"].longitude.value = first_result_location.lng();
     } else {
        alert("Geocode failed, placing new marker at centre of current map");
     }	
	document.forms["the_form"].submit();
}

function send_geocode_request() {	
	var trail_locality = document.forms["the_form"].trail_locality.value
	var new_point_place = document.forms["the_form"].place.value
	var new_point_address = new_point_place + ", " + trail_locality
    /*
    geocoder.geocode( 
    	{ 'address': new_point_address }, 
        handle_geocode_response
    );
    */
}

function add_item() {
    send_geocode_request();
}

