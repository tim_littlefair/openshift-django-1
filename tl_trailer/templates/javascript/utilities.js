/* utilities.js */
/* Copyright 2011, Tim and Jack Littlefair */

var map;
var place_name_element;
var latitude_element;
var longitude_element;
var first_result_location;
var zone_array = [];
var added_item_type;
var latitude_input;
var longitude_input;
var draggable_overlay;
var google;
var marker_image_prototype;
var circle_prototype;

function reinitialize() {
	google = top.google;
	var marker_image_prototype = new google.maps.MarkerImage(
		/* url */ "/static/annotation_zone.png",
		/* size */ null,
		/* origin */ null,
		/* anchor */ new google.maps.Point(10,10),
		/* scaledSize */ new google.maps.Size(20,20)
	)
	var circle_prototype = new google.maps.Circle({
		center: null,
		radius: null,
		fillColor: "#22ee22",
		fillOpacity: 0.2,
		strokeWeight: 1,
    	strokeColor: "#229922",
		strokeOpacity: 0.7,
		map: map
	});
}

function createOverlay(centre,radius) {
	reinitialize();
	var retval = circle_prototype;
	retval.center = centre;
	retval.radius = radius;
	return retval;
}

function add_zone(id,lat,long,radius) {
	var zone_details = [id,lat,long,radius];
    zone_array.push(zone_details); 
}

function marker_dragged(event) {
	var oldRadius = draggable_overlay.radius;
	draggable_overlay.setMap(null);
    latitude_input.value=event.latLng.lat()
    longitude_input.value= event.latLng.lng()
	draggable_overlay = createOverlay(event.latLng,oldRadius)
}

function initialize_map(latitude,longitude,zoom) {
	map = new google.maps.Map(
		document.getElementById("map_canvas"), 
		{
			zoom: zoom,
			center: new google.maps.LatLng(latitude,longitude),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		    disableDefaultUI: false,
		    /* 
		     * These don't work usefully yet but would 
		     * be good to get working.
		    mapTypeControl: true,
		    mapTypeControlOptions: {
		    	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
		    },
		    streetViewControl: true,
		    */
		}
	);
	place_name_element = document.getElementById("place_name");
	latitude_element = document.getElementById("latitude");
	longitude_element = document.getElementById("longitude");
	
	// If the map is displayed alongside the zone details
	// form, there should only be one marker and we want 
	// to make it draggable so that the details on the form
	// can be updated.
    var frameParent = window.frameElement.parentElement
    var detailsFrame = frameParent.parentElement.children[3].firstChild
    var detailsDoc = detailsFrame.contentDocument
    latitude_input = detailsDoc.getElementById('latitude')
    longitude_input = detailsDoc.getElementById('longitude')
    marker_draggable = false
	if( zone_array.length==1 && latitude_input != null && longitude_input != null) {
		marker_draggable = true
	}
	
	while(zone_array.length>0) {
		var zd = zone_array[0]
		if(marker_draggable==true) {
			var marker = new google.maps.Marker({
        		position: new google.maps.LatLng(zd[1],zd[2]),
        		title: zd[0],
        		draggable: true,
        		icon: marker_image,
        		map: map
    		});	
    		draggable_overlay = createOverlay(
    			new google.maps.LatLng(zd[1],zd[2]),zd[3])
			google.maps.event.addListener(marker,"dragend",marker_dragged)
		} 
		else
		{
			draggable_overlay = null
    		var non_draggable_overlay = createOverlay(
    			new google.maps.LatLng(zd[1],zd[2]),zd[3])
    	}
 		zone_array.shift();
	}
}


function popup_sharing_explanation() {
	var text = ""
	
	text += "Sharing levels for trails:\n\n"
	text += "private: The trail can only be accessed by the owner. "
	text += "The 'private' setting is only available for trails created by users who are logged in.\n\n"
	text += "linkable: The owner can allow other people to see or edit the trail by sharing one of the URLs which link to it. "
	text += "Each trail can be accessed via three different URLs, which give owner, editor and read-only rights.\n\n"
	text += "public: The owner can share the trail by sharing links as for the 'linkable' level, but is also "
	text += "willing to share read-only access to the trail publicly.  Trails can only be considered for "
	text += "inclusion in the 'Featured' group if the owner has declared them public in this way.\n\n"
	text += "Please check out the development blog for more details of how the sharing options work."
	
	alert(text)
}


