function restful_action(path, tqz_xml, method) {
    xhr = new XMLHttpRequest();
    xhr.open(method,path,false)
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("tqz_xml=" + escape(tqz_xml));
}

function create_trail() {
   var tqz_xml = ""
   tqz_xml = tqz_xml.concat("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
   tqz_xml = tqz_xml.concat("<tw1><tw1_plaintext/><tw1>\n");
   restful_action("/trail",tqz_xml,"POST")
}

function save_trail() {
	parent.setup_globals();
	var tqz_xml = ""
   	tqz_xml = tqz_xml.concat(
   		"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n",
   		"<tw1><tw1_plaintext ",
   		"handle=\"",parent.handle_input.value,"\" ",
   		"locality=\"",parent.locality_input.value,"\" ",
   		"description=\"",parent.description_input.value,"\" ",
   		"latitude=\"",parent.latitude_input.value,"\" ",
   		"longitude=\"",parent.longitude_input.value,"\" ",
   		"/></tw1>\n");
   	restful_action("/trail",tqz_xml,"PUT")
}
   